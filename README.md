**SHITPOST Generator Dokumentacija**

Svetainė sukurta su PHP naudojant MVC modelį. Su duombaze veiksmai atliekami panaudojant PDO.

Svetainės MVC branduolį sudarantys failai/struktūra:

* app katalogas - svtainės kodas (modeliai, controllers, views, nustatymai)
    * app/config/config.php - duomenų bazės kintamieji, svetainės pavadinimas, kelias
    * app/controllers - Čia dedami kontroleriai, kurie priskiria modelius, vaizdus, apdoroja GET, POST metodus
    * app/helpers - rašomos papildomos funkcijos, kurias norim panaudot.
    * app/libraries/Controller.php - Bazinė klasė, kurią paveldi visi kontroleriai, joje yra metodai gauti modeli, gauti view
    * app/libraries/Core.php - apdorojo URL, kad nusiųstų kontrolerius, kur reikia
    * app/libraries/Database.php - prijungia prie duombazės su PDO ir kiti PDO veiksmai
    * app/models - duomenų modeliai, atliekami veiksmai su duomenų baze (INSERT, SELECT ir t.t.)
    * app/views - kontrolerių vaizdai
    * app/views/includes/header.php - svetainės headeris
    * app/views/includes/footer.php - svetainės footeris
    * app/bootstrap.php - requirina bibliotekas, config failą.
    
* logs katalogas - saugomi serverio logs, jeigu įvyko klaida ir t.t.
* public katalogas - Index.php pradinis svetainės taškas + CSS/JSS/IMG failai
* 3 .htaccess failas skirti apdoroti nuorodas, kad jos būtų nukreiptus į tinkamus kontrolerius ir jų metodus

**Svetainės pradžia**

Svetainės root failas yra public/index.php panaudojant htaccess.

Pagrindinis kontroleris Pages.php, kuris turi index metodą.
Pagrinidis view yra app/views/pages/index.php

Kadangi pagrindinme puslapyje neatliekami veiksmai su duomenų baze, tai nereikia modelio

Kaip pasiekiame svetainę adresu http://localhost/shitpost-generator tada pagal .htaccess failą užkraunamas public.index.php failas,
kuris requirina app/bootstrap.php failą, kuris requirina svetainės nustatymus, pagrindines bibliotekas ir pagal defaultą nueina
į app/controllers/Pages.php failo index metodą, kuris sako, kad reikia užkrauti app/views/pages/index.php failą, kurį ir matome, 
kai nueinam į http://localhost/shitpost-generator.

Kitų linkų pvz. http://localhost/shitpost-generator/clickbaits/index - nueina į puslapį kur galime sukurt clickbeitus

Kontroleris gali turėti daug metodų, svarbu, kad metodo vardas sutaptų su views vardu, tam kad lenviau būtų galima 
atsekti, kuriį failą reikia redaguoti

**TRUMPAS MVC naujos funkcijos kūrimo pradžiamokslis**

Norint sukurti naują puslapį, pvz. norim sukurti clickbaitus.
1. Sukuriam naują controlerį pavadnimu Clickbaits.php, kurį saugom app/controller kataloge
2. Sukuriam metodą index Clickbaits.php faile, 
3. Sukuriam modelį Clickbait.php app/models kataloge, jame atliekami veiksmai su duombaze, kurie susiję su clickbaits duomenimis
4. Sukuriam app/views/clickbaits katalogą ir jame failą index.php
5. Kontroleryje loadinam modelį(Clickbait) ir view(clickbaits/index)
6. Nuėje į nuorodą localhost/shitpost-generator/clickbaits/index - matome view puslapį, kuriame gauname duomenis per kontrolerį iš modelio

**PALEIDIMAS lokaliame serverį naudojant XAMPP**

Reikia pakeist app/config/config.php duomenų bazės prisijungimo duomenis, kad atitiktų phpmyadmin domenis
Visi failai turi būti talpinami xampp/htdocs/shitpost-generator kataloge


