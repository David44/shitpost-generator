<?php

  function checkIfuserLikedClickbait($user_id, $clickbait_id) {
    $db = new Database();

    $db->query("SELECT * FROM clickbait_likes WHERE user_id = :user_id AND clickbait_id = :clickbait_id");
    $db->bind(":user_id", $user_id);
    $db->bind(":clickbait_id", $clickbait_id);

    $row = $db->SingleResult();
    $rows = $db->getRowCount();
    return $rows;
  }



  function generateClickbaitNoun($clickbaitWord, $singleOrPlural) {
    $facts = rand(2, 99);
    $people = ['Albert Einstein', 'Bill Gates', 'Steve Jobs', 'Socrates', 'Joseph Stalin', 'Napoleon' ,'Winston Churchill', 'Abraham Lincoln'];
    $factType = ['shocking', 'mind blowing', 'brave', 'amazing', 'spectacular', 'intelligent', 'intense', 'surprising'];

    $randomPeople = $people[array_rand($people)];
    $randomFactType = $factType[array_rand($factType)];
    // plural nouns
    if ($singleOrPlural == 'plural') {

      $nounPluralClickbaits = [
        'What We Found about # Was ' . $randomFactType,
        'Big Companies Hate Him, see what one guy learned about #',
        'You’ll Never Guess Why # ' . $randomFactType,
        'When you read These ' . $facts . ' ' . $randomFactType . ' # facts. You’ll Never Want to eat again',
        'This ' . $randomFactType . ' # footage will shake even the most Skeptic non-believer',
        $facts . 'thing they don’t tell you about #',
        'What This Brilliant Scientist ' . $randomPeople . ' Discovered about # Will Change Your Habits',
        'How # are the Answers to Middle East Peace',
        $facts .' ways to explain # to kids',
        'Where # are Headed in the Next Five Years',
        'The ' . $facts . ' worst songs about #',
        $facts . ' ways to fight ' . $randomPeople . ' using only #',
        'How to build an Empire with #',
        'Why ' .$randomPeople .' was right about #',
        $facts . ' life lessons learned from #',
        'How Hollywood got # all wrong',
        'How # made me a better person',
        'The '. $facts . ' most ' .$randomFactType .' # of all time',
        'You will never believe what ' . $randomPeople . ' said about #',
        $facts .' surprising ways # will be different in ' . $facts . ' years',
        $facts . ' ways men try to use # to get laid',
        $facts . ' wrong facts about # that everyone thinks are true',
        'Why # sucks: reality vs myth'  
      ];

      // $bodytag = str_replace("%body%", "black", "<body text='%body%'>");
      $randomClickbait = $nounPluralClickbaits[array_rand($nounPluralClickbaits)];
      $finalCLickbait = str_replace("#", $clickbaitWord, $randomClickbait);
  
      return $finalCLickbait;

    }
    else {
      $nounSingleClickbaits = [
        'What We Found about # Was ' . $randomFactType,
        'Big Companies Hate Him, see what one guy ' . $randomPeople . ' learned about #',
        'Think this is just a normal # ? Just wait until you see what’s inside',
        'This ' . $randomFactType .' # footage will shake even the most Skeptic non-believer',
        'What this # can do do to survive is actually pretty genius',
        $facts . ' things they don’t tell you about #',
        'If You Can Watch This # movie And Not Feel Surprised, Then You\'re Made Of Stone',
        $facts . ' Tips That Will Keep You From Becoming a #',
        'This Forgotten # Made Me Stop Giving To Charities',
        'This # Made Me Re-examine My Life',
        '10 Tips That Will Keep You From Becoming a #',
        'Why ' .$randomPeople .' was right about #',
        $facts . ' Facts About # that will impress your friends',
        $facts . ' secrets about # the Government is hiding.',
        $facts .' surprising ways # will be different in ' . $facts . ' years',
        $facts .' conspiracy theories about # that might actually be true',
        $facts .' ways to explain # to kids',
        $facts . ' ways men try to use # to get laid',
        $facts . ' wrong facts about # that everyone thinks are true',
        $randomPeople . 'and the untold story of #',
        $facts .' reasons why # get better with alcohol'
      ];

      // $bodytag = str_replace("%body%", "black", "<body text='%body%'>");
      $randomClickbait = $nounSingleClickbaits[array_rand($nounSingleClickbaits)];
      $finalCLickbait = str_replace("#", $clickbaitWord, $randomClickbait);
  
      return $finalCLickbait;
    }
  }

  function generateClickbaitAdjective($clickbaitWord) {
    $facts = rand(2, 99);
    $animals = ['cats', 'dogs', 'parrots', 'horses', 'chickens', 'parrots', 'rabbits', 'zebras'];
    $people = ['Albert Einstein', 'Bill Gates', 'Steve Jobs', 'Socrates', 'Joseph Stalin', 'Napoleon' ,'Winston Churchill', 'Abraham Lincoln'];
    $randomPeople = $people[array_rand($people)];
    $randomAnimal = $animals[array_rand($animals)];
    
    $adjectiveClickbaits = [
      'The '. $facts . ' most # people of all time',
      'The '. $facts . ' most # videos of all time',
      'Read most # story about ' . $randomPeople . 'in a long time',
      'What We Found about ' . $randomPeople. ' Was #',
      'You’ll Never Guess Why ' . $randomAnimal .' are #',
      'When you read These ' . $facts . ' # ' . $randomAnimal . ' facts. You’ll Never Want to eat again',
      'This # footage will shake even the most Skeptic non-believer',
      'The ' . $facts . ' # songs about ' . $randomPeople,
      'How '. $randomPeople . ' made me a # person',
      $facts . ' reasons you\'d want to be stuck in an elavator with '. $randomPeople
    ];

    // $bodytag = str_replace("%body%", "black", "<body text='%body%'>");
    $randomClickbait = $adjectiveClickbaits[array_rand($adjectiveClickbaits)];
    $finalCLickbait = str_replace("#", $clickbaitWord, $randomClickbait);

    return $finalCLickbait;
  }