<?php
class ClickbaitComment {
    private $db; // duomenu baze

    // Sukuriam nauja PDO prisjungima prie duomenu bazes
    public function __construct() {
        $this->db = new Database;
    }

    // Grazinti visus komentarus, kurie priklauso clickbaitui
    public function getClickbaitCommentsById($id) {
      
      $this->db->query("SELECT *, 
                        clickbaits_comments.id as commentId,
                        users.id as userId,
                        clickbaits_comments.created_at as commentCreated,
                        users.created_at as userCreated
                        FROM clickbaits_comments
                        INNER JOIN users
                        on clickbaits_comments.user_id = users.id
                        WHERE clickbaits_comments.clickbait_id = :id
                        ORDER BY clickbaits_comments.created_at DESC");

      $this->db->bind(':id', $id);             

      $results = $this->db->getResults();

      return $results;
    }

    // Grazinti viena komentara pagal jo ID
    public function getCommentById($id) {
        $this->db->query('SELECT * FROM clickbaits_comments WHERE id = :id');
        $this->db->bind(':id', $id);
    
        $row = $this->db->singleResult();
    
        return $row;
    }

    public function addCommentToClickbait($data) {
      $this->db->query('INSERT INTO clickbaits_comments (comment, clickbait_id, user_id) VALUES (:comment, :clickbait_id, :user_id)');
  
      // bind values
      $this->db->bind(':comment', $data['comment']);
      $this->db->bind(':clickbait_id', $data['clickbait_id']);
      $this->db->bind(':user_id', $data['user_id']);

      // execute query
      if ($this->db->execute()) {
        return true;
      }
      else {
        return false;
      }
    }

    // Grazinti kiek is viso yra clickbait komentaru
    public function getClickbaitCommentsCountById($clickbait_id) {
      $this->db->query('SELECT * FROM clickbaits_comments WHERE clickbait_id = :clickbait_id');
      $this->db->bind(':clickbait_id', $clickbait_id);

      $row = $this->db->getResults();

      //Check row
      $rowCount = $this->db->getRowCount();

      return $rowCount;
    }

    // Istrinti komentara
    public function deleteComment($comment_id) {
      $this->db->query('DELETE FROM clickbaits_comments WHERE id = :id');
    
      // bind values
      $this->db->bind(':id', $comment_id);
    
      // execute query
      if ($this->db->execute()) {
        return true;
      }
      else {
         return false;
      }
    }

}