<?php
class Deepfry {
    private $db; // duomenu baze

    // Sukuriam nauja PDO prisjungima prie duomenu bazes
    public function __construct() {
        $this->db = new Database;
    }

    // Gauti clickbaitus pagal nurodyta puslapi($page) ir kiek tame puslapyje yra clickabaitu ($perPage)
    public function getDeepfriedByPage($page, $perPage) {
        $startPosition = ($page > 1) ? ($page * $perPage) - $perPage : 0;

        $this->db->query("SELECT *, 
                          deepfried.id as deepfriedId,
                          deepfried.path as deepfriedPath,
                          users.id as userId,
                          deepfried.created_at as deepfriedCreated,
                          users.created_at as userCreated
                          FROM deepfried
                          INNER JOIN users
                          on deepfried.user_id = users.id
                          ORDER BY deepfried.created_at DESC
                          LIMIT $startPosition, $perPage");

        $results = $this->db->getResults();

        return $results;
    }

    // Suskaiciuojam kiek is viso yra clickbaitu
    public function getDeepfriedRowsTotalPages($perPage) {
        $this->db->query("SELECT *
                          FROM deepfried");

        $results = $this->db->getResults();

        $totalRows = $this->db->getRowCount();
        $pages = ceil($totalRows / $perPage);

        return $pages;
    }

    // Gaunam clickbaita pagal jo id
    public function getDeepfriedById($id) {
        $this->db->query('SELECT * FROM deepfried WHERE id = :id');
        $this->db->bind(':id', $id);

        $row = $this->db->singleResult();

        return $row;
    }


    // Get my(user) all clickbaits
    public function GetMyDeepfried($user_id) {
        $this->db->query("SELECT * 
                          FROM deepfried
                          WHERE user_id = :user_id
                          ORDER BY created_at DESC");

        $this->db->bind(':user_id', $user_id);

        $results = $this->db->getResults();

        return $results;
    }

    // Istrinti clickbaita
    public function deleteDeepfried($deepfried_id, $path) {
        $this->db->query('DELETE FROM deepfried WHERE id = :id');

        // bind values
        $this->db->bind(':id', $deepfried_id);

        // execute query
        if ($this->db->execute()) {

            if(unlink(UNROOT.$path)){
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    public function saveDeepfry($data){
        header('Content-Type: text/plain; charset=utf-8');

        try {

            // Undefined | Multiple Files | $_FILES Corruption Attack
            // If this request falls under any of them, treat it invalid.
            if (
                !isset($data['photo']['error']) ||
                is_array($data['photo']['error'])
            ) {
                throw new RuntimeException('Invalid parameters.');
            }

            // Check $_FILES['upfile']['error'] value.
            switch ($data['photo']['error']) {
                case UPLOAD_ERR_OK:
                    break;
                case UPLOAD_ERR_NO_FILE:
                    throw new RuntimeException('No file sent.');
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    throw new RuntimeException('Exceeded filesize limit.');
                default:
                    throw new RuntimeException('Unknown errors.');
            }

            // You should also check filesize here.
            if ($data['photo']['size'] > 1000000) {
                throw new RuntimeException('Exceeded filesize limit.');
            }

            // DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
            // Check MIME Type by yourself.
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            if (false === $ext = array_search(
                    $finfo->file($data['photo']['tmp_name']),
                    array(
                        'jpg' => 'image/jpeg',
                        'png' => 'image/png',
                        'gif' => 'image/gif',
                    ),
                    true
                )) {
                throw new RuntimeException('Invalid file format.');
            }

            $count = $this->db->query('SELECT count(*) AS c FROM deepfried');
            $size = $this->db->singleResult();

            echo $size['c'];

            if($size['c'] > 15000) {
                throw new RuntimeException('Server overloaded. Too much shitposting. Please donate for better servers :(');
            }

            $path = sprintf(IMG_UPLOAD_ROOT,
                sha1_file($data['photo']['tmp_name']),
                $ext
            );

            $path2 = sha1_file($data['photo']['tmp_name']).".".$ext;

            // You should name it uniquely.
            // DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
            // On this example, obtain safe unique name from its binary data.
            if (!move_uploaded_file(
                $data['photo']['tmp_name'],
                $path
            )) {
                echo $_SERVER['DOCUMENT_ROOT'];
                throw new RuntimeException('Failed to move uploaded file.');
            }
            echo 'File is uploaded successfully.';


            $this->db->query("INSERT INTO deepfried (path, user_id, likes) VALUES (:path, :user_id, :likes)");

            $this->db->bind(":path", $path2);
            $this->db->bind(":user_id", $data['user']);
            $this->db->bind(":likes", 0);

            // execute query
            if ($this->db->execute()) {
                return true;
            }
            else {
                return false;
            }

        } catch (RuntimeException $e) {

            echo $e->getMessage();

        }
    }
}