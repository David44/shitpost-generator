<?php
class ClickbaitLike {
    private $db; // duomenu baze

    // Sukuriam nauja PDO prisjungima prie duomenu bazes
    public function __construct() {
        $this->db = new Database;
    }

    public function getUserLike($clickbait_id, $user_id) {
      $this->db->query("SELECT * FROM clickbait_likes WHERE user_id = :user_id AND clickbait_id = :clickbait_id");

      $this->db->bind('user_id', $user_id);
      $this->db->bind('clickbait_id', $clickbait_id);

      $row = $this->db->SingleResult();

      //Check row
      $rowCount = $this->db->getRowCount();

      return $rowCount;
    }

    public function insertUserLike($clickbait_id, $user_id) {

      $this->db->query("INSERT INTO clickbait_likes (user_id, clickbait_id) VALUES (:user_id, :clickbait_id)");
      $this->db->bind(":user_id", $user_id);
      $this->db->bind(":clickbait_id", $clickbait_id);

      // execute query
      if ($this->db->execute()) {
        return true;
      }
      else {
        return false;
      }

    }

    public function DeleteUserLike($clickbait_id, $user_id) {

      $this->db->query("DELETE FROM clickbait_likes WHERE user_id = :user_id AND clickbait_id = :clickbait_id");
      $this->db->bind(":user_id", $user_id);
      $this->db->bind(":clickbait_id", $clickbait_id);

        // execute query
        if ($this->db->execute()) {
          return true;
        }
        else {
          return false;
        }

    }

    public function updateLikeCount($likes, $clickbait_id) {
      $this->db->query("UPDATE clickbaits SET likes = :likes WHERE id = :clickbait_id");
      $this->db->bind(":likes", (int) $likes);
      $this->db->bind(":clickbait_id", $clickbait_id);

      // execute query
      if ($this->db->execute()) {
        return true;
      }
      else {
        return false;
      }

    }

    public function deleteClickbaitLikes($clickbait_id) {
      $this->db->query("DELETE FROM clickbait_likes WHERE clickbait_id = :clickbait_id");
      $this->db->bind(":clickbait_id", $clickbait_id);

      // execute query
      if ($this->db->execute()) {
        return true;
      }
      else {
        return false;
      }
    }

}