<?php

class User {
    private $db;

    public function __construct() {
        $this->db = new Database;
    }

    // Login with accouunt
    public function login($username, $password) {
        $this->db->query('SELECT * FROM users WHERE username = :username');
        $this->db->bind(':username', $username);

        $row = $this->db->singleResult();

        $hashedPassword = $row['password'];
        if (password_verify($password, $hashedPassword)) {
            return $row;
        }
        else {
            return false;
        }
    }

    // Register account
    public function register($data) {

        // set default user level
        $default_user_level = 'user';
        $default_user_status = 'active';

        $this->db->query('INSERT INTO users (username, email, password, user_level, status) VALUES (:username, :email, :password, :user_level, :status)');

        // bind values
        $this->db->bind(':username', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);
        $this->db->bind(':user_level', $default_user_level);
        $this->db->bind(':status', $default_user_status);

        // execute query
        if ($this->db->execute()) {
            return true;
        }
        else {
            return false;
        }
    }

    // Find user by email
    public function findUserByEmail($email) {
        $this->db->query('SELECT * FROM users WHERE email = :email');
        $this->db->bind(':email', $email);

        $row = $this->db->singleResult();

        //Check row
        if ($this->db->getRowCount() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    // Find user by username
    public function findUserByUsername($username) {
        $this->db->query('SELECT * FROM users WHERE username = :username');
        $this->db->bind(':username', $username);

        $row = $this->db->singleResult();

        //Check row
        if ($this->db->getRowCount() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    // Get user by username
    public function getUser($username) {
        $this->db->query('SELECT * FROM users WHERE username = :username');
        $this->db->bind(':username', $username);

        $row = $this->db->singleResult();

        return $row;
    }

    // Return all users
    public function getAllUsers() {
        $this->db->query('SELECT * FROM users');

        $rows = $this->db->getResults();

        return $rows;
    }
    
    // Get user user by id
    public function getUserById($id) {

        $this->db->query('SELECT * FROM users WHERE id = :id');
        $this->db->bind(':id', $id);
      
        $row = $this->db->singleResult();
      
        return $row;
    }

    // Patikrinti ar vartotojas nera uzbanintas
    public function isUserActive($status) {
        if ($status == 'active') {
            return true;
        }
        else {
            return false;
        }
    }

    // Uzbanininti vartotoja pagal jo id
    public function banUserById($user_id) {
        $this->db->query("UPDATE users SET status = :status WHERE id = :user_id");
        $this->db->bind(":status", 'banned');
        $this->db->bind(":user_id", $user_id);
  
        // execute query
        if ($this->db->execute()) {
          return true;
        }
        else {
          return false;
        }
    }

    // atbaninti vartotoja pagal jo id
    public function unbanUserById($user_id) {
        $this->db->query("UPDATE users SET status = :status WHERE id = :user_id");
        $this->db->bind(":status", 'active');
        $this->db->bind(":user_id", $user_id);
  
        // execute query
        if ($this->db->execute()) {
          return true;
        }
        else {
          return false;
        }
    }

    // patikrinti ar ivestas slaptazodis sutampa su senu
    public function checkOldPassword($username, $password) {
        $user = $this->getUser($username);

        $hashedPassword = $user['password'];
        if (password_verify($password, $hashedPassword)) {
            return true;
        }
        else {
            return false;
        }
    }

    // Vartotojo profilio redagavimo uzklausa
    public function editUser($data) {

        $userId = $data['id'];

        $this->db->query("UPDATE users SET username = :username, email = :email, password = :password WHERE id = $userId");

        // bind values
        $this->db->bind(':username', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);

        // execute query
        if ($this->db->execute()) {
            $_SESSION['user_email'] = $data['email'];
            $_SESSION['user_username'] = $data['name'];
            return true;
        }
        else {
            return false;
        }
    }
}