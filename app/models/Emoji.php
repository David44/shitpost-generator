﻿<?php
class Emoji {
    private $db; // duomenu baze

    // Sukuriam nauja PDO prisjungima prie duomenu bazes
    public function __construct() {
        $this->db = new Database;
    }

    // Gauti emoji tekstus pagal nurodyta puslapi($page) ir kiek tame puslapyje yra emoji tekstu ($perPage)
    public function getEmojifiedTextsByPage($page, $perPage) {
        $startPosition = ($page > 1) ? ($page * $perPage) - $perPage : 0;

        $this->db->query("SELECT *, 
                          emojify.id as emojifiedText_id,
                          users.id as user_id,
						  emojify.created_at as emojifiedTextCreated
                          FROM emojify
                          INNER JOIN users
                          on emojify.user_id = users.id
						  ORDER BY emojify.created_at DESC
                          LIMIT $startPosition, $perPage");

        $results = $this->db->getResults();

        return $results;
    }

    // Suskaiciuojam kiek is viso yra emojify'intu tekstu
    public function getEmojifiedTextsRowsTotalPages($perPage) {
        $this->db->query("SELECT *
                          FROM emojify");

        $results = $this->db->getResults();

        $totalRows = $this->db->getRowCount();
        $pages = ceil($totalRows / $perPage);

        return $pages;
    }

    // Gaunam emojify'inta teksta pagal jo id
    public function getEmojifiedTextById($id) {
        $this->db->query('SELECT * FROM emojify WHERE id = :id');
        $this->db->bind(':id', $id);
  
        $row = $this->db->singleResult();
  
        return $row;
    }

    // Pridedam emojify'inta teksta i duomenu baze
    public function addEmojifiedText($data) {
        $this->db->query('INSERT INTO emojify (emojifiedText, user_id, likes) VALUES (:emojifiedText, :user_id, 0)');
  
        //$likes = 0;
        // bind values
        $this->db->bind(':emojifiedText', $data['emojifiedText']);
        $this->db->bind(':user_id', $data['user_id']);
        //$this->db->bind(':likes', (int)$likes);
  
        // execute query
        if ($this->db->execute()) {
          return true;
        }
        else {
          return false;
        }
    }

    // Get my(user) all emojified texts
    public function GetMyEmojifiedTexts($user_id) {
        $this->db->query("SELECT * 
                          FROM emojify
                          WHERE user_id = :user_id
                          ORDER BY created_at DESC");

        $this->db->bind(':user_id', $user_id);                  

        $results = $this->db->getResults();

        return $results;
    }

    // Gauti visus emojified tekstus, kurie atitinka duota raktazodi
    public function searchEmojifiedText($keyword) {

        $likeKeyword = '%' . $keyword . '%';

        $this->db->query("SELECT * 
        FROM emojify
        WHERE emojifiedText LIKE :likeKeyword
        ORDER BY created_at DESC"); 
        
        $this->db->bind(':likeKeyword', $likeKeyword);

        $results = $this->db->getResults();

        return $results;
    }

    // Istrinti Emojifyinta teksta
    public function deleteEmojifiedText($emojifiedText_id) {
        $this->db->query('DELETE FROM emojify WHERE id = :id');
  
        // bind values
        $this->db->bind(':id', $emojifiedText_id);
  
        // execute query
        if ($this->db->execute()) {
          return true;
        }
        else {
          return false;
        }
    }

    // Atnaujint emojifyinta teksta
    public function updateEmojifiedText($data) {
        $this->db->query('UPDATE emojify SET emojifiedText = :text WHERE id = :id');
  
        // bind values
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':text', $data['emojifiedText']);
  
        // execute query
        if ($this->db->execute()) {
          return true;
        }
        else {
          return false;
        }
    }

}