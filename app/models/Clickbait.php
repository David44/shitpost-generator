<?php
class Clickbait {
    private $db; // duomenu baze

    // Sukuriam nauja PDO prisjungima prie duomenu bazes
    public function __construct() {
        $this->db = new Database;
    }

    // Gauti clickbaitus pagal nurodyta puslapi($page) ir kiek tame puslapyje yra clickabaitu ($perPage)
    public function getClickbaitsByPage($page, $perPage) {
        $startPosition = ($page > 1) ? ($page * $perPage) - $perPage : 0;

        $this->db->query("SELECT *, 
                          clickbaits.id as clickbaitId,
                          users.id as userId,
                          clickbaits.created_at as clickbaitCreated,
                          users.created_at as userCreated
                          FROM clickbaits
                          INNER JOIN users
                          on clickbaits.user_id = users.id
                          ORDER BY clickbaits.created_at DESC
                          LIMIT $startPosition, $perPage");

        $results = $this->db->getResults();

        return $results;
    }

    // Suskaiciuojam kiek is viso yra clickbaitu
    public function getClickbaitsRowsTotalPages($perPage) {
        $this->db->query("SELECT *
                          FROM clickbaits");

        $results = $this->db->getResults();

        $totalRows = $this->db->getRowCount();
        $pages = ceil($totalRows / $perPage);

        return $pages;
    }

    // Gaunam clickbaita pagal jo id
    public function getClickbaitById($id) {
        $this->db->query('SELECT * FROM clickbaits WHERE id = :id');
        $this->db->bind(':id', $id);
  
        $row = $this->db->singleResult();
  
        return $row;
    }

    // Pridedam clickbait i duomenu baze
    public function addClickbait($data) {
        $this->db->query('INSERT INTO clickbaits (clickbait, user_id, likes) VALUES (:clickbait, :user_id, :likes)');
  
        $likes = 0;
        // bind values
        $this->db->bind(':clickbait', $data['clickbait']);
        $this->db->bind(':user_id', $data['user_id']);
        $this->db->bind(':likes', (int)$likes);
  
        // execute query
        if ($this->db->execute()) {
          return true;
        }
        else {
          return false;
        }
    }

    // Get my(user) all clickbaits
    public function GetMyClickbaits($user_id) {
        $this->db->query("SELECT * 
                          FROM clickbaits
                          WHERE user_id = :user_id
                          ORDER BY created_at DESC");

        $this->db->bind(':user_id', $user_id);                  

        $results = $this->db->getResults();

        return $results;
    }

    // Gauti visus clikcbaitus, kurie atitinka duota raktazodi
    public function searchClickbaits($keyword) {

        $likeKeyword = '%' . $keyword . '%';

        $this->db->query("SELECT * 
        FROM clickbaits
        WHERE clickbait LIKE :likeKeyword
        ORDER BY created_at DESC"); 
        
        $this->db->bind(':likeKeyword', $likeKeyword);

        $results = $this->db->getResults();

        return $results;
    }

    // Istrinti clickbaita
    public function deleteClickbait($clickbait_id) {
        $this->db->query('DELETE FROM clickbaits WHERE id = :id');
  
        // bind values
        $this->db->bind(':id', $clickbait_id);
  
        // execute query
        if ($this->db->execute()) {
          return true;
        }
        else {
          return false;
        }
    }

    // Atnaujint clickbaita
    public function updateClickbait($data) {
        $this->db->query('UPDATE clickbaits SET clickbait = :clickbait WHERE id = :id');
  
        // bind values
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':clickbait', $data['clickbait']);
  
        // execute query
        if ($this->db->execute()) {
          return true;
        }
        else {
          return false;
        }
    }

}