<?php
class DeepfryLike {
    private $db; // duomenu baze

    // Sukuriam nauja PDO prisjungima prie duomenu bazes
    public function __construct() {
        $this->db = new Database;
    }

    public function getUserLike($deepfried_id, $user_id) {
        $this->db->query("SELECT * FROM deepfried_likes WHERE user_id = :user_id AND deepfry_id = :deepfry_id");

        $this->db->bind(":user_id", $user_id);
        $this->db->bind(":deepfry_id", $deepfried_id);

        $row = $this->db->SingleResult();

        //Check row
        $rowCount = $this->db->getRowCount();

        return $rowCount;
    }

    public function insertUserLike($deepfried_id, $user_id) {

        $this->db->query("INSERT INTO deepfried_likes (user_id, deepfry_id) VALUES (:user_id, :deepfry_id)");
        $this->db->bind(":user_id", $user_id);
        $this->db->bind(":deepfry_id", $deepfried_id);

        // execute query
        if ($this->db->execute()) {
            return true;
        }
        else {
            return false;
        }

    }

    public function DeleteUserLike($deepfried_id, $user_id) {

        $this->db->query("DELETE FROM deepfried_likes WHERE user_id = :user_id AND deepfry_id = :deepfry_id");
        $this->db->bind(":user_id", $user_id);
        $this->db->bind(":deepfry_id", $deepfried_id);

        // execute query
        if ($this->db->execute()) {
            return true;
        }
        else {
            return false;
        }

    }

    public function updateLikeCount($likes, $deepfried_id) {
        $this->db->query("UPDATE deepfried SET likes = :likes WHERE id = :deepfry_id");
        $this->db->bind(":likes", (int) $likes);
        $this->db->bind(":deepfry_id", $deepfried_id);

        // execute query
        if ($this->db->execute()) {
            return true;
        }
        else {
            return false;
        }

    }

    public function deleteDeepfriedLikes($deepfried_id) {
        $this->db->query("DELETE FROM deepfried_likes WHERE deepfry_id = :deepfry_id");
        $this->db->bind(":deepfry_id", $deepfried_id);

        // execute query
        if ($this->db->execute()) {
            return true;
        }
        else {
            return false;
        }
    }

}