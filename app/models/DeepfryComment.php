<?php
class DeepfryComment {
    private $db; // duomenu baze

    // Sukuriam nauja PDO prisjungima prie duomenu bazes
    public function __construct() {
        $this->db = new Database;
    }

    // Grazinti visus komentarus, kurie priklauso clickbaitui
    public function getDeepfriedCommentsById($id) {

        $this->db->query("SELECT *,
                        deepfried_comments.id as commentId,
                        users.id as userId,
                        deepfried_comments.created_at as commentCreated,
                        users.created_at as userCreated
                        FROM deepfried_comments
                        INNER JOIN users
                        on deepfried_comments.user_id = users.id
                        WHERE deepfried_comments.deepfry_id = :id
                        ORDER BY deepfried_comments.created_at DESC");

        $this->db->bind(':id', $id);

        $results = $this->db->getResults();

        return $results;
    }

    // Grazinti viena komentara pagal jo ID
    public function getCommentById($id) {
        $this->db->query('SELECT * FROM deepfried_comments WHERE id = :id');
        $this->db->bind(':id', $id);

        $row = $this->db->singleResult();

        return $row;
    }

    public function addCommentToDeepfried($data) {
        $this->db->query('INSERT INTO deepfried_comments (comment, deepfry_id, user_id) VALUES (:comment, :deepfry_id, :user_id)');

        // bind values
        $this->db->bind(':comment', $data['comment']);
        $this->db->bind(':deepfry_id', $data['deepfry_id']);
        $this->db->bind(':user_id', $data['user_id']);

        // execute query
        if ($this->db->execute()) {
            return true;
        }
        else {
            return false;
        }
    }

    // Grazinti kiek is viso yra clickbait komentaru
    public function getDeepfriedCommentsCountById($deepfried_id) {
        $this->db->query('SELECT * FROM deepfried_comments WHERE deepfry_id = :deepfry_id');
        $this->db->bind(':deepfry_id', $deepfried_id);

        $row = $this->db->getResults();

        //Check row
        $rowCount = $this->db->getRowCount();

        return $rowCount;
    }

    // Istrinti komentara
    public function deleteComment($comment_id) {
        $this->db->query('DELETE FROM deepfried_comments WHERE id = :id');

        // bind values
        $this->db->bind(':id', $comment_id);

        // execute query
        if ($this->db->execute()) {
            return true;
        }
        else {
            return false;
        }
    }

}