<?php
class EmojiComment {
    private $db; // duomenu baze

    // Sukuriam nauja PDO prisjungima prie duomenu bazes
    public function __construct() {
        $this->db = new Database;
    }

    // Grazinti visus komentarus, kurie priklauso emojifyintui tekstui
    public function getEmojifiedtextCommentsById($id) {
      
      $this->db->query("SELECT *, 
                        emojis_comments.id as commentId,
                        users.id as userId,
                        emojis_comments.created_at as commentCreated,
                        users.created_at as userCreated
                        FROM emojis_comments
                        INNER JOIN users
                        on emojis_comments.user_id = users.id
                        WHERE emojis_comments.emojifiedText_id = :id
                        ORDER BY emojis_comments.created_at DESC");

      $this->db->bind(':id', $id);             

      $results = $this->db->getResults();

      return $results;
    }

    // Grazinti viena komentara pagal jo ID
    public function getCommentById($id) {
        $this->db->query('SELECT * FROM emojis_comments WHERE id = :id');
        $this->db->bind(':id', $id);
    
        $row = $this->db->singleResult();
    
        return $row;
    }

    public function addCommentToEmojifiedText($data) {
      $this->db->query('INSERT INTO emojis_comments (comment, emojifiedText_id, user_id) VALUES (:comment, :emojifiedText_id, :user_id)');
  
      // bind values
      $this->db->bind(':comment', $data['comment']);
      $this->db->bind(':emojifiedText_id', $data['emojifiedText_id']);
      $this->db->bind(':user_id', $data['user_id']);

      // execute query
      if ($this->db->execute()) {
        return true;
      }
      else {
        return false;
      }
    }

    // Grazinti kiek is viso yra emoji komentaru
    public function getEmojifiedTextCommentsCountById($emojifiedText_id) {
      $this->db->query('SELECT * FROM emojis_comments WHERE emojifiedText_id = :emojifiedText_id');
      $this->db->bind(':emojifiedText_id', $emojifiedText_id);

      $row = $this->db->getResults();

      //Check row
      $rowCount = $this->db->getRowCount();

      return $rowCount;
    }

    // Istrinti komentara
    public function deleteComment($comment_id) {
      $this->db->query('DELETE FROM emojis_comments WHERE id = :id');
    
      // bind values
      $this->db->bind(':id', $comment_id);
    
      // execute query
      if ($this->db->execute()) {
        return true;
      }
      else {
         return false;
      }
    }

}