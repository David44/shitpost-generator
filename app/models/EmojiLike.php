<?php
class EmojiLike {
    private $db; // duomenu baze

    // Sukuriam nauja PDO prisjungima prie duomenu bazes
    public function __construct() {
        $this->db = new Database;
    }

    public function getUserLike($emojifiedText_id, $user_id) {
      $this->db->query("SELECT * FROM emoji_likes WHERE user_id = :user_id AND emojifiedText_id = :emojifiedText_id");

      $this->db->bind('user_id', $user_id);
      $this->db->bind('emojifiedText_id', $emojifiedText_id);

      $row = $this->db->SingleResult();

      //Check row
      $rowCount = $this->db->getRowCount();

      return $rowCount;
    }

    public function insertUserLike($emojifiedText_id, $user_id) {

      $this->db->query("INSERT INTO emoji_likes (user_id, emojifiedText_id) VALUES (:user_id, :emojifiedText_id)");
      $this->db->bind(":user_id", $user_id);
      $this->db->bind(":emojifiedText_id", $emojifiedText_id);

      // execute query
      if ($this->db->execute()) {
        return true;
      }
      else {
        return false;
      }

    }

    public function DeleteUserLike($emojifiedText_id, $user_id) {

      $this->db->query("DELETE FROM emoji_likes WHERE user_id = :user_id AND emojifiedText_id = :emojifiedText_id");
      $this->db->bind(":user_id", $user_id);
      $this->db->bind(":emojifiedText_id", $emojifiedText_id);

        // execute query
        if ($this->db->execute()) {
          return true;
        }
        else {
          return false;
        }

    }

    public function updateLikeCount($likes, $emojifiedText_id) {
      $this->db->query("UPDATE emojify SET likes = :likes WHERE id = :emojifiedText_id");
      $this->db->bind(":likes", (int) $likes);
      $this->db->bind(":emojifiedText_id", $emojifiedText_id);

      // execute query
      if ($this->db->execute()) {
        return true;
      }
      else {
        return false;
      }

    }

    public function deleteEmojifiedTextLikes($emojifiedText_id) {
      $this->db->query("DELETE FROM emoji_likes WHERE emojifiedText_id = :emojifiedText_id");
      $this->db->bind(":emojifiedText_id", $emojifiedText_id);

      // execute query
      if ($this->db->execute()) {
        return true;
      }
      else {
        return false;
      }
    }

}