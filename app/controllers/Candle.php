<?php
class Candle extends Controller {

    public function index($url_params) {
        $data = [
            'title' => 'Candle'
        ];

        $this->view('candle/index', $data);
    }
    
        public function generate() {
 $data = [
            'title' => 'Candle'
        ];

        $this->view('candle/generate', $data);
    }

        public function create() {
 $data = [
            'title' => 'Candle'
        ];

        $this->view('candle/create', $data);
    }

        public function build() {
 $data = [
            'title' => 'Candle'
        ];

        $this->view('candle/build', $data);
    }
}
?>