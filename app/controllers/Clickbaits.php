<?php
class Clickbaits extends Controller {

    // Clickbait konstruktorius, priskiria norimus metodus
    public function __construct() {
        $this->clickbaitModel = $this->model('Clickbait');
        $this->clickbaitCommentModel = $this->model('ClickbaitComment');
        $this->userModel = $this->model('User');
        $this->clickbaitLikeModel = $this->model('ClickbaitLike');
    }

    // Praidinis clickbaitu puslapis, gaunami visi clickbaitai su puslapiaimu
    public function index($url_params) {
        $page = 1; // default page
        $perPage = 10;
        if (isset($url_params[0])) {
            $page = (int) $url_params[0];
        }

        $pages = $this->clickbaitModel->getClickbaitsRowsTotalPages($perPage);

        // Validate pages bounds
        if ($page > $pages){
            $page = (int) $pages;
        }
        else if ($page <= 0) {
            $page = (int) 1;
        }

        $clickbaits = $this->clickbaitModel->getClickbaitsByPage($page, $perPage);

        $data = [
            'title' => 'Clickbaits',
            'clickbaits' => $clickbaits,
            'curentPage' => $page,
            'pages' => $pages
        ];

        $this->view('clickbaits/index', $data);
    }

    // Perziureti tam tikra clickbaita pagal jo ID
    public function show($url_params) {
        // turi but nurodytas clickbaito id
        if (isset($url_params[0])) {
            $clickbait_id = (int) $url_params[0];
        }
        else {
            redirect('clickbaits/index/1');
        }

        $clickbait = $this->clickbaitModel->getClickbaitById($clickbait_id);
        $user = $this->userModel->getUserById($clickbait['user_id']);
        $comments = $this->clickbaitCommentModel->getClickbaitCommentsById($clickbait_id);

        $userClickbaitLike = $this->clickbaitLikeModel->getUserLike($clickbait_id, $clickbait['user_id']);

        // jeigu clickbaitas nerastas, tai griztam i pradini puslapi
        if ($clickbait['clickbait'] == ''){
            redirect('clickbaits/index/1');
        }
        else {
            $data = [
                'clickbait' => $clickbait,
                'user' => $user,
                'comments' => $comments,
                'comment' => '',
                'getLikeValue' => $userClickbaitLike
            ];

            $this->view('clickbaits/show', $data);
        }

    }

    // Parodyti visus mano clickbaitus
    public function showMyClickbaits() {
        if (isLoggedIn()) { 
            $clickbaits = $this->clickbaitModel->GetMyClickbaits($_SESSION['user_id']);

            $data = [
                'clickbaits' => $clickbaits
            ];

            $this->view('clickbaits/showMyClickbaits', $data);
        }
        else {
            redirect('clickbaits/index/1');
        }
    }

    public function like() {

        if (isset($_POST['liked'])) {

                $clickbait_id = $_POST['clickbaitid'];
                $user_id = $_POST['userid'];

                $clickbait = $this->clickbaitModel->getClickbaitById($clickbait_id);
                $likes = $clickbait['likes'];

                $userVoted = checkIfuserLikedClickbait($user_id, $clickbait_id);

                if ($userVoted == 1) {
                    echo $likes;
                    exit();
                }
                else if ($userVoted == 0) {
                    if ($this->clickbaitLikeModel->insertUserLike($clickbait_id, $user_id) && $this->clickbaitLikeModel->updateLikeCount($likes + 1, $clickbait_id))
                    {
                        echo $likes + 1;
                        exit();
                    }
                }
                else {
                    echo $likes;
                    exit();
                }
        }
    }

    public function unlike() {

        if (isset($_POST['unliked'])) {

                $clickbait_id = $_POST['clickbaitid'];
                $user_id = $_POST['userid'];

                $clickbait = $this->clickbaitModel->getClickbaitById($clickbait_id);
                $likes = $clickbait['likes'];

                $userVoted = checkIfuserLikedClickbait($user_id, $clickbait_id);

                if ($userVoted == 0) {
                    echo $likes;
                    exit();
                }
                else if ($userVoted == 1) {
                    if ($this->clickbaitLikeModel->DeleteUserLike($clickbait_id, $user_id) && $this->clickbaitLikeModel->updateLikeCount($likes - 1, $clickbait_id)) {
                        echo $likes - 1;
                        exit();
                    }
                }              
                else {
                    echo $likes;
                    exit();
                }
                
        }
    }

    // Sukurti nauja clickbaita su pasirinkta antraste
    public function create() {
        if (isLoggedIn()) { 
            if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                // sanitize the clickbait
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
                $data = [
                  'clickbait' => trim($_POST['clickbait']),
                  'user_id' => $_SESSION['user_id'],
                  'clickbait_error' => ''
                ];
        
                // validation
                // validate the clickbait title
                if (empty($data['clickbait'])) {
                  $data['clickbait_error'] = 'Please enter a clicbait title';     
                }
        
                // Make sure theare no errorrs
                if (empty($data['clickbait_error'])) {
        
                  // Validated input
                  if ($this->clickbaitModel->addClickbait($data)){
                    flash('clickbait_message', 'New clickbait created.');
                    redirect('clickbaits/index/1');
                  } else {
                        $this->view('clickbaits/create', $data);
                  }
                }
                else {
                  // Load the view with erros
                  $this->view('clickbaits/create', $data);
                }
        
              }
              else {
                $data = [
                  'title' => '',
                  'clickbait' => ''
                ];
          
                $this->view('clickbaits/create', $data);
              }
        }
        else {
            redirect('clickbaits/index/1');
        }
    }

    // Clickbaito sugeneravimas
    public function generate() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'title' => 'Generate New clickbait',
                'clickbaitWord' => trim($_POST['clickbaitWord']),
                'generatedClickbait' => '',
                'verbOrNoun' => trim($_POST['optionsRadios']),
                'singleOrPluralNoun' => trim($_POST['ExtraOptionsRadios']),
                'clickbait_error' => ''
            ];

            // validation
            // validate the clickbait title
            if (empty($data['clickbaitWord'])) {
                $data['clickbait_error'] = 'Please enter a clickbait Word';     
            }

            if (empty($data['clickbait_error'])) {

                if ($data['verbOrNoun'] == 'verbCheck') {
                    $data['generatedClickbait'] = generateClickbaitAdjective($data['clickbaitWord']);
                }
                else {
                    $data['generatedClickbait'] = generateClickbaitNoun($data['clickbaitWord'], $data['singleOrPluralNoun']);
                }

                $this->view('clickbaits/generate', $data);
            }
            else {
                $this->view('clickbaits/generate', $data);
            }
        }

        else {
            $data = [
                'title' => 'Generate New clickbait',
                'generatedClickbait' => '',
                'clickbaitWord' => '',
                'verbOrNoun' => 'nounCheck',
                'singleOrPluralNoun' => 'single',
                'clickbait_error' => ''
            ];

            $this->view('clickbaits/generate', $data);
        }
    }

    // Clickbaito sugeneravimas
    public function search() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){

            // sanitize string
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'clickbaitSearch' => trim($_POST['clickbaitSearch']),
                'clickbaitSearch_error' => '',
                'clickbaits' => []
            ];

            if (empty($data['clickbaitSearch'])) {
                $data['clickbaitSearch_error'] = 'Please enter a search query';
            }

            // Make sure theare no errorrs
            if (empty($data['clickbaitSearch_error'])) {

                // Gauti surastus rezultatus
                $searchResults = $this->clickbaitModel->searchClickbaits($data['clickbaitSearch']);

                // Rezultatus priskirti i clickbaits masyva
                $data['clickbaits'] = $searchResults;

                // Pavaizduoti duomenis
                $this->view('clickbaits/search', $data);
                }
            else {
                // Load the view with erros
                $this->view('clickbaits/search', $data);
            }

        }
        else
        {
            $data = [
                'clickbaits' => [],
                'clickbaitSearch' => ''
            ];

            $this->view('clickbaits/search', $data);
        }

    }

    // Sukurti nauja komentarai nuorydtam clickbaitui
    public function createComment($url_params) {
        // turi but nurodytas clickbaito id
        if (isset($url_params[0])) {
            $clickbait_id = (int) $url_params[0];
        }
        else {
            redirect('clickbaits/index/1');
        }

        if (isLoggedIn()) { 
            if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                // sanitize the clickbait
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
                $data = [
                  'comment' => trim($_POST['comment']),
                  'user_id' => $_SESSION['user_id'],
                  'clickbait_id' => (int)$clickbait_id,
                  'comment_error' => ''
                ];
        
                // validation
                // validate the clickbait comment
                if (empty($data['comment'])) {
                    redirect('clickbaits/show/' . $clickbait_id);   
                }
        
                // Make sure theare no errorrs
                if (empty($data['comment_error'])) {
        
                  // Validated input
                  if ($this->clickbaitCommentModel->addCommentToClickbait($data)){
                    flash('clickbait_comment_message', 'New comment posted.');
                    redirect('clickbaits/show/' . $clickbait_id);
                  } else {
                        redirect('clickbaits/show/' . $clickbait_id);
                  }
                }
                else {
                  redirect('clickbaits/show/' . $clickbait_id);
                }
        
              }
              else {
                redirect('clickbaits/show/' . $clickbait_id);
              }
        }
        else {
            redirect('clickbaits/show/' . $clickbait_id);
        }
    }

    public function edit($url_params) {

        $clickbait_id = $url_params[0];

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){        
          // sanitize the clickbait
          $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
  
          $data = [
            'clickbait' => trim($_POST['clickbait']),
            'user_id' => $_SESSION['user_id'],
            'id' => $clickbait_id,
            'clickbait_error' => ''
          ];
  
          // validation
          // validate the clickabit
          if (empty($data['clickbait'])) {
            $data['clickbait_error'] = 'Please enter a clickbait title';     
          }
  
          // Make sure theare no errorrs
          if (empty($data['clickbait_error'])) {
  
            // Validated input
            if ($this->clickbaitModel->updateClickbait($data)){
              flash('clickbait_message', 'Clikcbait is now updated.');
              redirect('clickbaits/index/1');
            } else {
                $this->view('clickbaits/edit', $data);
            }
          }
          else {
            // Load the view with erros
            $this->view('clickbaits/edit', $data);
          }
  
        }
        else {
          // Get existing clickbait
          $clickbait = $this->clickbaitModel->getClickbaitById($clickbait_id);
          // Check for owner
          if ($clickbait['user_id'] != $_SESSION['user_id']){
            redirect('clickbaits');
          }
          $data = [
            'id' => $clickbait_id,
            'clickbait' => $clickbait['clickbait']
          ];
    
          $this->view('clickbaits/edit', $data);
        }
    }

    public function delete($url_params) {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

          $clickbait_id = $url_params[0];  
  
          // Get existing clickbait
          $clickbait = $this->clickbaitModel->getClickbaitById($clickbait_id);
          // Check for owner
          if ($clickbait['user_id'] != $_SESSION['user_id']){
             redirect('clickbaits/index/1');
          }

          $clickbaitCommentsTotal = $this->clickbaitCommentModel->getClickbaitCommentsCountById($clickbait_id);

          if ($clickbaitCommentsTotal > 0) {           
            flash('clickbait_message', 'Clickbait cannot be removed because it has comments!', 'alert alert-danger');
            redirect('clickbaits/index/1');
          }
          else {
            if ($this->clickbaitModel->deleteClickbait($clickbait_id)) {
                // Tai pat panaikinam ir clickbaito laikus
                $this->clickbaitLikeModel->deleteClickbaitLikes($clickbait_id);
                flash('clickbait_message', 'Clickbait successfully removed');
                redirect('clickbaits/index/1');
              }
              else {
                redirect('clickbaits/index/1');
              }
          }
        }
        else {
          redirect('clickbaits/index/1');
        }
    }

    public function deleteComment($url_params) {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $comment_id = $url_params[0];  
            $clickbait_id = $url_params[2];
            // Get existing comment
            $comment = $this->clickbaitCommentModel->getCommentById($comment_id);
            // Check for owner
            if ($comment['user_id'] != $_SESSION['user_id']){
                redirect('clickbaits/index/1');
            }

            if ($this->clickbaitCommentModel->deleteComment($comment_id)) {
                flash('clickbait_comment_message', 'Comment successfully removed');
                redirect('clickbaits/show/' . $clickbait_id);
            }
            else {
                redirect('clickbaits/index/1');
            }
        }
        else {
            redirect('clickbaits/index/1');
        }
    }

}