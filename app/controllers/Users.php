<?php

class Users extends Controller {

    // Priskiramas varotojo modelis
    public function __construct() {
        $this->userModel = $this->model('User');
    }

    // Varotoju registracija
    public function register() {
        if (isLoggedIn() == false) {
            // patikrinti ar post
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                // process the form

                // Sanitize POST data
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $data = [
                    'name' => trim($_POST['name']),
                    'email' => trim($_POST['email']),
                    'password' => trim($_POST['password']),
                    'confirm_password' => trim($_POST['confirm_password']),
                    'name_error' => '',
                    'email_error' => '',
                    'password_error' => '',
                    'confirm_password_error' => ''
                ];

                // Validate email
                if (empty($data['email'])) {
                    $data['email_error'] = 'Please enter email';
                } else {
                    // Check if email exists
                    if ($this->userModel->findUserByEmail($data['email'])) {
                        $data['email_error'] = 'This email already exists';
                    }
                }

                // Validate username
                if (empty($data['name'])) {
                    $data['name_error'] = 'Please enter username';
                } else {
                    // Check if email exists
                    if ($this->userModel->findUserByUsername($data['name'])) {
                        $data['name_error'] = 'This username already exists';
                    }
                }

                // Validate password
                if (empty($data['password'])) {
                    $data['password_error'] = 'Please enter password';
                } else if (strlen($data['password']) < 6) {
                    $data['password_error'] = 'Password must be atleast 6 characters';
                }

                // Validate confimr password
                if (empty($data['confirm_password'])) {
                    $data['confirm_password_error'] = 'Please confirm password';
                } else {
                    if ($data['password'] != $data['confirm_password']) {
                        $data['confirm_password_error'] = 'Passwords do not match';
                    }
                }

                // Make sure errors are empty
                if (empty($data['email_error']) && empty($data['name_error']) && empty($data['password_error']) && empty($data['confirm_password_error'])) {
                    // Validted

                    // HASH user password
                    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

                    // Register user
                    if ($this->userModel->register($data)) {
                        flash('register_success', 'You are now registered! Log in.');
                        redirect('users/login');
                    } else {
                        die ('Something went wrong.');
                    }
                } else {
                    // Load the view with errors
                    $this->view('users/register', $data);
                }
            } else {
                // load the form
                // init data
                $data = [
                    'name' => '',
                    'email' => '',
                    'password' => '',
                    'confirm_password' => '',
                    'name_error' => '',
                    'email_error' => '',
                    'password_error' => '',
                    'confirm_password_error' => ''
                ];

                // load view
                $this->view('users/register', $data);

            }
        }
        else {
            redirect('pages/index');
        }
    }

    // Vartotoju prisijungimas
    public function login() {
        if (isLoggedIn() == false) {
            // patikrinti ar post
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                // process the form

                // Sanitize POST data
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $data = [
                    'username' => trim($_POST['username']),
                    'password' => trim($_POST['password']),
                    'username_error' => '',
                    'password_error' => ''
                ];

                // Validate username
                if (empty($data['username'])) {
                    $data['username_error'] = 'Please enter your username';
                }

                // Validate password
                if (empty($data['password'])) {
                    $data['password_error'] = 'Please enter password';
                }

                // Cehck for username
                if ($this->userModel->findUserByUsername($data['username'])) {
                    // User exists
                } else {
                    $data['username_error'] = 'User not found';
                }

                // Make sure errors are empty
                if (empty($data['username_error']) && empty($data['password_error'])) {
                    // Validted
                    // Check and set logged in user
                    $loggedInUser = $this->userModel->login($data['username'], $data['password']);

                    if ($loggedInUser) {
                        // Create new session
                        if ($this->userModel->isUserActive($loggedInUser['status'])) {
                            $this->createUserSession($loggedInUser);
                        }
                        else {
                            $data['username_error'] = 'YOUR USER IS BANNED!';
                            $this->view('users/login', $data);
                        }
                    } else {
                        $data['password_error'] = 'Password incorrect';
                        $this->view('users/login', $data);
                    }
                } else {
                    // Load the view with errors
                    $this->view('users/login', $data);
                }

            } else {
                // load the form
                // init data
                $data = [
                    'username' => '',
                    'password' => '',
                    'username_error' => '',
                    'password_error' => ''
                ];

                // load view
                $this->view('users/login', $data);

            }
        }
        else {
            redirect('pages/index');
        }
    }

    // Vartotojo nustatymai
    public function editUser() {
        if (isLoggedIn()) {
            // POST metodas formos updeitui
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                // process the form

                // Sanitize POST data
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $data = [
                    'name' => trim($_POST['name']),
                    'email' => trim($_POST['email']),
                    'old_password' => $_POST['old_password'],
                    'password' => trim($_POST['password']),
                    'confirm_password' => trim($_POST['confirm_password']),
                    'name_error' => '',
                    'email_error' => '',
                    'old_password_error' => '',
                    'password_error' => '',
                    'confirm_password_error' => ''
                ];

                // Validate email
                if (empty($data['email'])) {
                    $data['email_error'] = 'Please enter email';
                } else {
                    // Check if email exists
                    if ($this->userModel->findUserByEmail($data['email'])) {
                        if (($data['email']) != $_SESSION['user_email'])
                            $data['email_error'] = 'This email already exists';
                    }
                }

                // Validate username
                if (empty($data['name'])) {
                    $data['name_error'] = 'Please enter username';
                } else {
                    // Check if email exists
                    if ($this->userModel->findUserByUsername($data['name'])) {
                        if (($data['name']) != $_SESSION['user_username'])
                            $data['name_error'] = 'This username already exists';
                    }
                }
                // Validate old password
                if (empty($data['old_password'])) {
                    $data['old_password_error'] = 'Please enter your current password';
                } else {
                    $checkOldPassword = $this->userModel->checkOldPassword($_SESSION['user_username'], $data['old_password']);

                    if (!$checkOldPassword) {
                        // Create new session
                        $data['old_password_error'] = 'Your old Password is incorrect';
                        $this->view('users/editUser', $data);
                    }
                }

                // Validate password
                if (empty($data['password'])) {
                    $data['password_error'] = 'Please enter new password';
                } else if (strlen($data['password']) < 6) {
                    $data['password_error'] = 'Password must be atleast 6 characters';
                } else if ($data['old_password'] == $data['password']) {
                    $data['password_error'] = 'Current and new passwords must be different!';
                }

                // Validate confimr password
                if (empty($data['confirm_password'])) {
                    $data['confirm_password_error'] = 'Please confirm new password';
                } else {
                    if ($data['password'] != $data['confirm_password']) {
                        $data['confirm_password_error'] = 'Passwords do not match';
                    }
                }

                // Make sure errors are empty
                if (empty($data['email_error']) && empty($data['name_error']) && empty($data['password_error']) && empty($data['confirm_password_error']) && empty($data['old_password_error'])) {
                    // Validted

                    // HASH user password
                    $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
                    $data['id'] = $_SESSION['user_id'];

                    // update user
                    if ($this->userModel->editUser($data)) {
                        flash('editUser_success', 'Successfully edited user details.');
                        redirect('users/editUser');
                    } else {
                        die ('Something went wrong.');
                    }
                } else {
                    // Load the view with errors
                    $this->view('users/editUser', $data);
                }
            } // GET METODAS, atvaziduojam orma
            else {
                // load the form
                // init data
                $data = [
                    'name' => $_SESSION['user_username'],
                    'email' => $_SESSION['user_email'],
                    'old_password' => '',
                    'password' => '',
                    'confirm_password' => '',
                    'name_error' => '',
                    'email_error' => '',
                    'old_password_error' => '',
                    'password_error' => '',
                    'confirm_password_error' => ''
                ];

                // load view
                $this->view('users/editUser', $data);

            }
        }
        else {
            redirect('pages/index');
        }
    }

    // Varotojo profilio perziurejimas pagal jo id
    public function show($url_params) {
        $user = $this->userModel->getUserById($url_params[0]);

        $data = [
            'user' => $user
        ];

        $this->view('users/show', $data);

    }

    // Vartotoju sarasas, galima tik adminstratoriui
    public function manageUsers() {

        if (isLoggedIn() && $_SESSION['user_userlevel'] == adminLevel) {
            $users = $this->userModel->getAllUsers();

            $data = [
                'users' => $users
            ];

            $this->view('users/manageUsers', $data);
        }

        else {
            redirect('pages');
        }
    }

    // Vartotojo baninimo kontroleris
    public function banUser($url_params) {
        $user_id = $url_params[0];

        if (isLoggedIn() && $_SESSION['user_userlevel'] == adminLevel && $user_id != $_SESSION['user_id']) {

            if ($user = $this->userModel->banUserById($user_id)){
                redirect('users/manageUsers');
            }
            else {
                redirect('users/manageUsers');
            }

        }
        else {
            redirect('pages');
        }
    }

    // Vartotojo atbaninimo kontroleris
    public function unbanUser($url_params) {
        $user_id = $url_params[0];

        if (isLoggedIn() && $_SESSION['user_userlevel'] == adminLevel && $user_id != $_SESSION['user_id']) {

            if ($user = $this->userModel->unbanUserById($user_id)){
                redirect('users/manageUsers');
            }
            else {
                redirect('users/manageUsers');
            }

        }
        else {
            redirect('pages');
        }
    }

    // Naujos vartotojo sesijos sukurimas, kai vartotojas prisijungia
    public function createUserSession($user) {
        $_SESSION['user_id'] = $user['id'];
        $_SESSION['user_email'] = $user['email'];
        $_SESSION['user_username'] = $user['username'];
        $_SESSION['user_userlevel'] = $user['user_level'];
        redirect('pages/index');
    }

    // Vartotojo atsijungimo mygtukas, sunaikinama varotojo sesija
    public function logout() {
        unset($_SESSION['user_id']);
        unset($_SESSION['user_email']);
        unset($_SESSION['user_username']);
        unset($_SESSION['user_userlevel']);
        session_destroy();
        redirect('users/login');
    }



}