<?php
class Deepfried extends Controller {

    // Deepfry konstruktorius, priskiria norimus metodus
    public function __construct() {
        $this->DeepfryModel = $this->model('Deepfry');
        $this->DeepfryCommentModel = $this->model('DeepfryComment');
        $this->userModel = $this->model('User');
        $this->DeepfryLikeModel = $this->model('DeepfryLike');
    }

    //Main page, get photos, order by page
    public function index($url_params) {
        $page = 1; // default page
        $perPage = 10;
        if (isset($url_params[0])) {
            $page = (int) $url_params[0];
        }

        $pages = $this->DeepfryModel->getDeepfriedRowsTotalPages($perPage);

        // Validate pages bounds
        if ($page > $pages){
            $page = (int) $pages;
        }
        else if ($page <= 0) {
            $page = (int) 1;
        }

        $deepfried = $this->DeepfryModel->getDeepfriedByPage($page, $perPage);

        $data = [
            'title' => 'Images',
            'deepfried' => $deepfried,
            'curentPage' => $page,
            'pages' => $pages
        ];

        $this->view('deepfried/index', $data);
    }

    //Look at a particular image
    public function show($url_params) {
        //Image id must be sets
        if (isset($url_params[0])) {
            $deepfried_id = (int) $url_params[0];
        }
        else {
            redirect('deepfried/index/1');
        }

        $deepfried = $this->DeepfryModel->getDeepfriedById($deepfried_id);
        $user = $this->userModel->getUserById($deepfried['user_id']);
        $comments = $this->DeepfryCommentModel->getDeepfriedCommentsById($deepfried_id);
        $userDeepfriedLike = $this->DeepfryLikeModel->getUserLike($deepfried_id, $deepfried['user_id']);

        //If no image exists simply redirect to main page
        if ($deepfried['path'] == ''){
            redirect('deepfried/index/1');
        }
        else {
            $data = [
                'deepfried' => $deepfried,
                'user' => $user,
                'comments' => $comments,
                'comment' => '',
                'getLikeValue' => $userDeepfriedLike
            ];

            $this->view('deepfried/show', $data);
        }

    }

    //Get my deepfried images
    public function showMyDeepfried() {
        if (isLoggedIn()) {
            $deepfried = $this->DeepfryModel->GetMyDeepfried($_SESSION['user_id']);

            $data = [
                'deepfried' => $deepfried
            ];

            $this->view('deepfried/showMyDeepfried', $data);
        }
        else {
            redirect('deepfried/index/1');
        }
    }

    //User likes deepfried image
    public function like() {

        if (isset($_POST['liked'])) {

            $deepfried_id = $_POST['deepfriedId'];
            $user_id = $_POST['userid'];

            $deepfried = $this->DeepfryModel->getDeepfriedById($deepfried_id);
            $likes = $deepfried['likes'];

            $userVoted = checkIfuserLikedDeepfried($user_id, $deepfried_id);

            if ($userVoted == 1) {
                echo $likes;
                exit();
            }
            else if ($userVoted == 0) {
                if ($this->DeepfryLikeModel->insertUserLike($deepfried_id, $user_id) && $this->DeepfryLikeModel->updateLikeCount($likes + 1, $deepfried_id))
                {
                    echo $likes + 1;
                    exit();
                }
            }
            else {
                echo $likes;
                exit();
            }
        }
    }

    //User unliked a deepfry image
    public function unlike() {

        if (isset($_POST['unliked'])) {

            $deepfried_id = $_POST['deepfriedId'];
            $user_id = $_POST['userid'];

            $deepfried = $this->DeepfryModel->getDeepfriedById($deepfried_id);
            $likes = $deepfried['likes'];

            $userVoted = checkIfuserLikedDeepfried($user_id, $deepfried_id);

            if ($userVoted == 0) {
                echo $likes;
                exit();
            }
            else if ($userVoted == 1) {
                if ($this->DeepfryLikeModel->DeleteUserLike($deepfried_id, $user_id) && $this->DeepfryLikeModel->updateLikeCount($likes - 1, $deepfried_id)) {
                    echo $likes - 1;
                    exit();
                }
            }
            else {
                echo $likes;
                exit();
            }

        }
    }

    //Publish new deepfry
    public function create() {
        if (isLoggedIn()) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                // sanitize the clickbait
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $data = [
                    'photo' => $_FILES['upfile'],
                    'user' => $_SESSION['user_id']
                ];

                if($this->DeepfryModel->saveDeepfry($data)){
                    redirect('deepfried/index/1');
                } else {
                    echo "Could not upload file.";
                }

            }
            else {
                $data = [
                    'title' => ''
                ];

                $this->view('deepfried/create', $data);
            }
        }
        else {
            redirect('deepfried/index/1');
        }
    }

    //Upload and edit a deepfried image
    public function generate() {

        $data = [
            'title' => 'Add effects to your image',
        ];

        $this->view('deepfried/generate', $data);
    }

    public function jpeg(){
        $data = [
            'title' => 'Add JPEG artifacts to your image',
        ];

        $this->view('deepfried/jpeg', $data);
    }

    //Post a comment for an image
    public function createComment($url_params) {
        //Image id must be set
        if (isset($url_params[0])) {
            $deepfried_id = (int) $url_params[0];
        }
        else {
            redirect('deepfried/index/1');
        }

        if (isLoggedIn()) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                //Sanitize
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
                $data = [
                    'comment' => trim($_POST['comment']),
                    'user_id' => $_SESSION['user_id'],
                    'deepfry_id' => (int)$deepfried_id,
                    'comment_error' => ''
                ];

                // validate the comment
                if (empty($data['comment'])) {
                    redirect('deepfried/show/' . $deepfried_id);
                }

                // Make sure there are no errors
                if (empty($data['comment_error'])) {

                    echo "epik";
                    // Validated input
                    if ($this->DeepfryCommentModel->addCommentToDeepfried($data)){
                        flash('deepfried_comment_message', 'New comment posted.');
                        redirect('deepfried/show/' . $deepfried_id);
                    } else {
                        redirect('deepfried/show/' . $deepfried_id);
                    }
                }
                else {
                    redirect('deepfried/show/' . $deepfried_id);
                }

            }
            else {
                redirect('deepfried/show/' . $deepfried_id);
            }
        }
        else {
            redirect('deepfried/show/' . $deepfried_id);
        }
    }

    //Delete an image
    public function delete($url_params) {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $deepfried_id = $url_params[0];

            //Get the image
            $deepfried = $this->DeepfryModel->getDeepfriedById($deepfried_id);
            $path = $deepfried['path'];
            // Check for owner
            if ($deepfried['user_id'] != $_SESSION['user_id']){
                redirect('deepfried/index/1');
            }

            if ($this->DeepfryModel->deleteDeepfried($deepfried_id,  $path)) {
                //Delete likes associated with the image
                $this->DeepfryLikeModel->deleteDeepfriedLikes($deepfried_id);
                flash('deepfry_message', 'Image successfully removed');
                redirect('deepfried/index/1');
            }
            else {
                redirect('deepfried/index/1');
            }
        }
        else {
            redirect('deepfried/index/1');
        }
    }

    //Delete a comment
    public function deleteComment($url_params) {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $comment_id = $url_params[0];
            $deepfried_id = $url_params[2];
            // Get existing comment
            $comment = $this->DeepfryCommentModel->getCommentById($comment_id);
            // Check for owner
            if ($comment['user_id'] != $_SESSION['user_id']){
                redirect('deepfried/index/1');
            }

            if ($this->DeepfryCommentModel->deleteComment($comment_id)) {
                flash('deepfried_comment_message', 'Comment successfully removed');
                redirect('deepfried/show/' . $deepfried_id);
            }
            else {
                redirect('deepfried/index/1');
            }
        }
        else {
            redirect('deepfried/index/1');
        }
    }

}