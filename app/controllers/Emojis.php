﻿<?php
class Emojis extends Controller {
	
	
	public function __construct() {
        $this->emojiModel = $this->model('Emoji');
        $this->emojiCommentModel = $this->model('EmojiComment');
        $this->userModel = $this->model('User');
        $this->emojiLikeModel = $this->model('EmojiLike');
    }
	
	
	public function index($url_params) {
        $page = 1; // default page
        $perPage = 10;
        if (isset($url_params[0])) {
            $page = (int) $url_params[0];
        }

        $pages = $this->emojiModel->getEmojifiedTextsRowsTotalPages($perPage);

        // Validate pages bounds
        if ($page > $pages){
            $page = (int) $pages;
        }
        else if ($page <= 0) {
            $page = (int) 1;
        }

        $emojifiedTexts = $this->emojiModel->getEmojifiedTextsByPage($page, $perPage);

        $data = [
            'title' => 'Emoji texts',
            'emojifiedTexts' => $emojifiedTexts,
            'curentPage' => $page,
            'pages' => $pages
        ];

        $this->view('emojis/index', $data);
    }
	
	// Perziureti tam tikra emojify'inta teksta pagal jo ID
    public function show($url_params) {
        // turi but nurodytas clickbaito id
        if (isset($url_params[0])) {
            $emojifiedText_id = (int) $url_params[0];
        }
        else {
            redirect('emojis/index/1');
        }

        $emojifiedText = $this->emojiModel->getEmojifiedTextById($emojifiedText_id);
        $user = $this->userModel->getUserById($emojifiedText['user_id']);
        $comments = $this->emojiCommentModel->getEmojifiedTextCommentsById($emojifiedText_id);

        $userEmojiLike = $this->emojiLikeModel->getUserLike($emojifiedText_id, $emojifiedText['user_id']);

        // jeigu emojifyintas tekstas nerastas, tai griztam i pradini puslapi
        if ($emojifiedText['emojifiedText'] == ''){
            redirect('emojis/index/1');
        }
        else {
            $data = [
                'emojifiedText' => $emojifiedText,
                'user' => $user,
                'comments' => $comments,
                'comment' => '',
                'getLikeValue' => $userEmojiLike
            ];

            $this->view('emojis/show', $data);
        }

    }
	
	// Parodyti visus mano clickbaitus
    public function showMyEmojifiedTexts() {
        if (isLoggedIn()) { 
            $emojifiedTexts = $this->emojiModel->GetMyEmojifiedTexts($_SESSION['user_id']);

            $data = [
                'emojifiedTexts' => $emojifiedTexts
            ];

            $this->view('emojis/showMyEmojifiedTexts', $data);
        }
        else {
            redirect('emojis/index/1');
        }
    }
	
	 public function like() {

        if (isset($_POST['liked'])) {

                $emojifiedText_id = $_POST['emojiid'];
                $user_id = $_POST['userid'];

                $emojifiedText = $this->emojiModel->getEmojifiedTextById($emojifiedText_id);
                $likes = $emojifiedText['likes'];

                $userVoted = checkIfuserLikedEmojifiedText($user_id, $emojifiedText_id);

                if ($userVoted == 1) {
                    echo $likes;
                    exit();
                }
                else if ($userVoted == 0) {
                    if ($this->emojiLikeModel->insertUserLike($emojifiedText_id, $user_id) && $this->emojiLikeModel->updateLikeCount($likes + 1, $emojifiedText_id))
                    {
                        echo $likes + 1;
                        exit();
                    }
                }
                else {
                    echo $likes;
                    exit();
                }
        }
    }
	
	public function unlike() {

        if (isset($_POST['unliked'])) {

                $emojifiedText_id = $_POST['emojiid'];
                $user_id = $_POST['userid'];

                $emojifiedText = $this->emojiModel->getEmojifiedTextById($emojifiedText_id);
                $likes = $emojifiedText['likes'];

                $userVoted = checkIfuserLikedEmojifiedText($user_id, $emojifiedText_id);

                if ($userVoted == 0) {
                    echo $likes;
                    exit();
                }
                else if ($userVoted == 1) {
                    if ($this->emojiLikeModel->DeleteUserLike($emojifiedText_id, $user_id) && $this->emojiLikeModel->updateLikeCount($likes - 1, $emojifiedText_id)) {
                        echo $likes - 1;
                        exit();
                    }
                }              
                else {
                    echo $likes;
                    exit();
                }
                
        }
    }
	
	public function create() {
        if (isLoggedIn()) { 
            if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                // sanitize the clickbait
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
                $data = [
                  'emojifiedText' => trim($_POST['emojifiedText']),
                  'user_id' => $_SESSION['user_id'],
                  'emojiText_error' => ''
                ];
        
                // validation
                // validate the emojified Text
                if (empty($data['emojifiedText'])) {
                  $data['emojiText_error'] = 'Please enter a text to emojify';     
                }
        
                // Make sure there are no errorrs
                if (empty($data['emojiText_error'])) {
        
                  // Validated input
                  if ($this->emojiModel->addEmojifiedText($data)){
                    flash('emojifiedText_message', 'New emojified text created.');
                    redirect('emojis/index/1');
                  } else {
                        $this->view('emojis/create', $data);
                  }
                }
                else {
                  // Load the view with erros
                  $this->view('emojis/create', $data);
                }
        
              }
              else {
                $data = [
                  'title' => '',
                  'emojifiedText' => ''
                ];
          
                $this->view('emojis/create', $data);
              }
        }
        else {
            redirect('emojis/index/1');
        }
    }
	
	 public function generate() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'title' => 'Generate New Emojified Text',
                'textToEmojify' => trim($_POST['textToEmojify']),
                'generatedEmojifiedtext' => '',
                //'verbOrNoun' => trim($_POST['optionsRadios']),
                //'singleOrPluralNoun' => trim($_POST['ExtraOptionsRadios']),
                'emojiText_error' => ''
            ];

            // validation
            // validate the emoji text title
            if (empty($data['textToEmojify'])) {
                $data['emojiText_error'] = 'Please enter a text to emojify';     
            }

            if (empty($data['emojiText_error'])) {

                
                
				$data['generatedEmojifiedText'] = getEmojifiedText($data['textToEmojify']);
                

                $this->view('emojis/generate', $data);
            }
            else {
                $this->view('emojis/generate', $data);
            }
        }

        else {
            $data = [
                'title' => 'Generate New Emojified Text',
                'generatedEmojifiedtext' => '',
                'textToEmojify' => '',
                //'verbOrNoun' => 'nounCheck',
                //'singleOrPluralNoun' => 'single',
                'emojiText_error' => ''
            ];

            $this->view('emojis/generate', $data);
        }
    }
	
	// emojifyinto teksto sugeneravimas
    public function search() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){

            // sanitize string
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'emojifiedTextSearch' => trim($_POST['emojifiedTextSearch']),
                'emojifiedTextSearch_error' => '',
                'emojifiedTexts' => []
            ];

            if (empty($data['emojifiedTextSearch'])) {
                $data['emojifiedTextSearch_error'] = 'Please enter a search query';
            }

            // Make sure theare no errors
            if (empty($data['emojifiedTextSearch_error'])) {

                // Gauti surastus rezultatus
                $searchResults = $this->emojiModel->searchEmojifiedText($data['emojifiedTextSearch']);

                // Rezultatus priskirti i emojified texts masyva
                $data['emojifiedTexts'] = $searchResults;

                // Pavaizduoti duomenis
                $this->view('emojis/search', $data);
                }
            else {
                // Load the view with erros
                $this->view('emojis/search', $data);
            }

        }
        else
        {
            $data = [
                'emojifiedTexts' => [],
                'emojifiedTextSearch' => ''
            ];

            $this->view('emojis/search', $data);
        }

    }
	
	// Sukurti nauja komentarai nurodytam emojifyintam tekstui
    public function createComment($url_params) {
        // turi but nurodytas emojifyinto teksto id
        if (isset($url_params[0])) {
            $emojifiedText_id = (int) $url_params[0];
        }
        else {
            redirect('emojis/index/1');
        }

        if (isLoggedIn()) { 
            if ($_SERVER['REQUEST_METHOD'] == 'POST'){
                // sanitize the emojified text
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
                $data = [
                  'comment' => trim($_POST['comment']),
                  'user_id' => $_SESSION['user_id'],
                  'emojifiedText_id' => (int)$emojifiedText_id,
                  'comment_error' => ''
                ];
        
                // validation
                // validate the emojified text comment
                if (empty($data['comment'])) {
                    redirect('emojis/show/' . $emojifiedText_id);   
                }
        
                // Make sure theare no errorrs
                if (empty($data['comment_error'])) {
        
                  // Validated input
                  if ($this->emojiCommentModel->addCommentToEmojifiedText($data)){
                    flash('emojifiedText_comment_message', 'New comment posted.');
                    redirect('emojis/show/' . $emojifiedText_id);
                  } else {
                        redirect('emojis/show/' . $emojifiedText_id);
                  }
                }
                else {
                  redirect('emojis/show/' . $emojifiedText_id);
                }
        
              }
              else {
                redirect('emojis/show/' . $emojifiedText_id);
              }
        }
        else {
            redirect('emojis/show/' . $emojifiedText_id);
        }
    }
	
	public function edit($url_params) {

        $emojifiedText_id = $url_params[0];

        if ($_SERVER['REQUEST_METHOD'] == 'POST'){        
          // sanitize the clickbait
          $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
  
          $data = [
            'emojifiedText' => trim($_POST['emojifiedText']),
            'user_id' => $_SESSION['user_id'],
            'id' => $emojifiedText_id,
            'emojifiedText_error' => ''
          ];
  
          // validation
          // validate the clickabit
          if (empty($data['emojifiedText'])) {
            $data['emojifiedText_error'] = 'Please enter a emojified text';     
          }
  
          // Make sure there are no errors
          if (empty($data['emojifiedText_error'])) {
  
            // Validated input
            if ($this->emojiModel->updateEmojifiedText($data)){
              flash('emojifiedText_message', 'Emojified text is now updated.');
              redirect('emojis/index/1');
            } else {
                $this->view('emojis/edit', $data);
            }
          }
          else {
            // Load the view with erros
            $this->view('emojis/edit', $data);
          }
  
        }
        else {
          // Get existing emojified text
          $emojifiedText = $this->emojiModel->getEmojifiedTextById($emojifiedText_id);
          // Check for owner
          if ($emojifiedText['user_id'] != $_SESSION['user_id']){
			  flash('emojifiedText_message', 'Emojified id differ.');
            redirect('emojis');
          }
          $data = [
            'id' => $emojifiedText_id,
            'emojifiedText' => $emojifiedText['emojifiedText']
          ];
    
          $this->view('emojis/edit', $data);
        }
    }
	
	public function delete($url_params) {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

          $emojifiedText_id = $url_params[0];  
  
          // Get existing emojified Text
          $emojifiedText = $this->emojiModel->getEmojifiedTextById($emojifiedText_id);
          // Check for owner
          if ($emojifiedText['user_id'] != $_SESSION['user_id']){
             redirect('emojis/index/1');
          }

          $emojifiedTextCommentsTotal = $this->emojiCommentModel->getEmojifiedTextCommentsCountById($emojifiedText_id);

          if ($emojifiedTextCommentsTotal > 0) {           
            flash('emojifiedText_message', 'Emojified Text cannot be removed because it has comments!', 'alert alert-danger');
            redirect('emojis/index/1');
          }
          else {
            if ($this->emojiModel->deleteEmojifiedText($emojifiedText_id)) {
                // Tai pat panaikinam ir emojifyinto teksto laikus
                $this->emojiLikeModel->deleteEmojifiedTextLikes($emojifiedText_id);
                flash('emojifiedText_message', 'Emojified Text successfully removed');
                redirect('emojis/index/1');
              }
              else {
                redirect('emojis/index/1');
              }
          }
        }
        else {
          redirect('emojis/index/1');
        }
    }
	
	 public function deleteComment($url_params) {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $comment_id = $url_params[0];  
            $emojifiedText_id = $url_params[2];
            // Get existing comment
            $comment = $this->emojiCommentModel->getCommentById($comment_id);
            // Check for owner
            if ($comment['user_id'] != $_SESSION['user_id']){
                redirect('emojis/index/1');
            }

            if ($this->emojiCommentModel->deleteComment($comment_id)) {
                flash('emojifiedText_comment_message', 'Comment successfully removed');
                redirect('emojis/show/' . $emojifiedText_id);
            }
            else {
                redirect('emojis/index/1');
            }
        }
        else {
            redirect('emojis/index/1');
        }
    }
}

?>



