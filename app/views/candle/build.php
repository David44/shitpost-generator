<?php require APPROOT . '/views/includes/header.php'; ?>
<h1><?php echo $data['title']; ?></h1>


<?php

if (isset($_POST['height'])) {
    $height = $_POST['height'];
    $width = $_POST['width'];
    $time = $_POST['time'];
    $color1 = $_POST['color1'];
    $color2 = $_POST['color2'];
    $fcolor1 = $_POST['fcolor1'];
    $fcolor2 = $_POST['fcolor2'];
    $fcolor3 = $_POST['fcolor3'];
    $fcolor3 = $_POST['fcolor4'];
    $res = 1920;
    $width_screen= (100 -(100 * ($width / $res))) /2;
    $cof1 = $width / 10;
    $cof2 = $cof1 + 10;
    $cof3 = $cof1 + 20;
    $sum = $cof3 * $cof3 * 2;
    $fwidth = sqrt($sum);
    $push = $width / 2 - $fwidth / 2;
}
else{
    $time = $_POST['time'];
    $height = (int) rand(20, 400);
    $width = (int) rand(50, 1000);
    $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
    $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
    $color1 =  $color;
    $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
    $color2 =  $color;
    $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
    $fcolor1 =  $color;
    $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
    $fcolor2 =  $color;
    $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
    $fcolor3 =  $color;
    $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
    $fcolor3 =  $color;
    $res = 1920;
    $width_screen= (100 -(100 * ($width / $res))) /2;
    $cof1 = $width / 10;
    $cof2 = $cof1 + 10;
    $cof3 = $cof1 + 20;
    $sum = $cof3 * $cof3 * 2;
    $fwidth = sqrt($sum);
    $push = $width / 2 - $fwidth / 2;
}
?>
<html>
    <head>
        <style>
            html, body {
                margin: 0;
                height: 100%;
                overflow: hidden;
                background: linear-gradient(white, white);
            }
            .candle {
                position: absolute;
                left: <?php echo $width_screen?>%;
                top: 50%;

            }
            .candle .top{
                margin:80px auto;
                height: 60px;
                left:<?php echo $push?>px;
                position:relative;
                transform-origin:center bottom;
                animation-name: flicker;
                animation-duration:3ms;
                animation-delay:200ms;
                animation-timing-function: ease-in;
                animation-iteration-count: infinite;
                animation-direction: alternate;
            }
            .candle .top .flame{
                bottom:0;
                position:absolute;
                border-bottom-right-radius: 50%;
                border-bottom-left-radius: 50%;
                border-top-left-radius: 50%;
                transform: translate3d(0px, -150px, 0px) rotate(-45deg) scale(1,1);
            }

            .candle .top .third{
                left:15px; 
                width: <?php echo $cof1 ?>px;
                height: <?php echo $cof1 ?>px;
                background:<?php echo $fcolor1 ?>;
                box-shadow: 0px 0px 9px 4px <?php echo $fcolor1 ?>;
            }

            .candle .top .second{
                left:10px; 
                width: <?php echo $cof2 ?>px;
                height:<?php echo $cof2 ?>px;
                background:<?php echo $fcolor2 ?>;
                box-shadow: 0px 0px 9px 4px <?php echo $fcolor2 ?>;
            }

            .candle .top .first{
                left:5px;
                width: <?php echo $cof3 ?>px;
                height: <?php echo $cof3 ?>px;
                background:<?php echo $fcolor3 ?>;
                box-shadow: 0px 0px 5px 4px <?php echo $fcolor3 ?>;
            }

            .candle .top .fourth{
                left:15px; 
                bottom:-4px;
                width: <?php echo $cof1 ?>px;
                height: <?php echo $cof1 ?>px;
                background:<?php echo $fcolor3 ?>;
                box-shadow: 0px 0px 9px 4px <?php echo $fcolor3 ?>;
            }

            .candle .roll {
                position: absolute;
                width: <?php echo $width; ?>px;
                height: <?php echo $height; ?>px;
                border-radius: 0% 0% -15% -15% / 0% 0% 50% 50%;
                background: linear-gradient(<?php echo $color1 ?>, <?php echo $color2 ?>);
                box-shadow: inset 0 0 1px rgba(0, 0, 0, 0.5), inset 0 5px 10px rgba(0, 0, 0, 0.3);
                animation: melt <?php echo $time; ?>s ease-in 2s both;
            }
            .candle .roll:before {
                position: absolute;
                content: "";
                height: 20%;
                width: 100%;
                top: -10%;
                border-radius: 50%;
                background: linear-gradient(<?php echo $color1 ?>, <?php echo $color2 ?>);
                box-shadow: inset 0 0 1px rgba(0, 0, 0, 0.5);
            }
            .candle .roll:after {
                position: absolute;
                content: "";
                height: 20%;
                width: 100%;
                top: 90%;
                border-radius: 50%;
                background: linear-gradient(<?php echo $color1 ?>, <?php echo $color2 ?>);
                box-shadow: inset 0 0 1px rgba(0, 0, 0, 0.5);
            }

            @keyframes flicker{
                0%   {transform: rotate(-1deg);}
                20%  {transform: rotate(1deg);}
                40%  {transform: rotate(-1deg);}
                60%  {transform: rotate(1deg) scaleY(1.04);}
                80%  {transform: rotate(-2deg) scaleY(0.92);}
                100% {transform: rotate(1deg) ;}
            }
            @keyframes melt {
                0% {
                }

                100% {
                    height : 20px;
                }
            }
        </style>
        <meta http-equiv="X-UA-Compatible" content="IE=9; text/html; charset=utf-8">

    </head>
    <body>
        <div class="candle">
            <div class="roll">           
                <div class="top">
                    <div class="first flame"></div>
                    <div class="second flame"></div>
                    <div class="third flame"></div>
                    <div class="fourth flame"></div>
                </div></div>

        </div>
    </body>
</html>
<?php require APPROOT . '/views/includes/footer.php'; ?>