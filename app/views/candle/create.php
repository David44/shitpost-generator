<?php require APPROOT . '/views/includes/header.php'; ?>
<h1><?php echo $data['title']; ?></h1>
<a href="<?php echo URLROOT; ?>/candle/index" class="btn btn-info">Back</a>

<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=9; text/html; charset=utf-8">
    </head>
    <body>
<div class="card card-body mt-4">
      <h3>Create your personal candle</h3>
        <form  action="<?php echo URLROOT;?>/candle/build" method="post">
            <div class="form-group row">
             <label for="inputHeight" class="col-sm-2 col-form-label">Height</label>
            <div class="col-sm-3">
      <input type="number" class="form-control" id="inputHeight" name="height" min="20" max="400" value="20">
    </div>
                            </div>
                 <div class="form-group row">
             <label for="inputWidth" class="col-sm-2 col-form-label">Width</label>
            <div class="col-sm-3">
      <input type="number" class="form-control" id="inputWidth" name="width" min="50" max="1000" value="50">
    </div>
            </div>
                             <div class="form-group row">
             <label for="inputTime" class="col-sm-2 col-form-label">Time</label>
            <div class="col-sm-3">
      <input type="number" class="form-control" id="inputTime" name="time" min="5" value="5">
    </div>
            </div> 
            <div class="form-group row">
             <label for="inputColor1" class="col-sm-2 col-form-label">Primary color</label>
            <div class="col-sm-3">
      <input type="color" class="form-control" id="inputColor1" name="color1" value="#ff9933">
    </div>
            </div>    
            <div class="form-group row">
             <label for="inputColor2" class="col-sm-2 col-form-label">Secondary color</label>
            <div class="col-sm-3">
      <input type="color" class="form-control" id="inputColor2" name="color2" value="#ffff66">
    </div>
            </div>
            <div class="form-group row">
             <label for="inputFColor1" class="col-sm-2 col-form-label">Primary flame color</label>
            <div class="col-sm-3">
      <input type="color" class="form-control" id="inputFColor1" name="fcolor1" value="#ff3300">
    </div>
            </div>  
            <div class="form-group row">
             <label for="inputFColor2" class="col-sm-2 col-form-label">Secondary flame color</label>
            <div class="col-sm-3">
      <input type="color" class="form-control" id="inputFColor2" name="fcolor2" value="#ff9933">
    </div>
            </div>  
            <div class="form-group row">
             <label for="inputFColor3" class="col-sm-2 col-form-label">Third flame color</label>
            <div class="col-sm-3">
      <input type="color" class="form-control" id="inputFColor3" name="fcolor3" value="#ffff66">
    </div>
            </div>  
            <div class="form-group row">
             <label for="inputFColor4" class="col-sm-2 col-form-label">Fourth flame color</label>
            <div class="col-sm-3">
      <input type="color" class="form-control" id="inputFColor4" name="fcolor4" value="#ffffff">
    </div>
            </div>  
            

            <input type="submit" value="Generate" class="btn btn-success"><br>
        </form>
        </div>
<?php require APPROOT . '/views/includes/footer.php'; ?>
    </body>
</html>

