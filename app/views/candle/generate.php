<?php require APPROOT . '/views/includes/header.php'; ?>
<h1><?php echo $data['title']; ?></h1>
<a href="<?php echo URLROOT; ?>/candle/index" class="btn btn-info">Back</a>

<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=9; text/html; charset=utf-8">
    </head>
    <body>
<div class="card card-body mt-4">
      <h3>Generate your personal candle</h3>
        <form  action="<?php echo URLROOT;?>/candle/build" method="post">
             <div class="form-group row">
             <label for="inputTime" class="col-sm-2 col-form-label">Time</label>
            <div class="col-sm-3">
      <input type="number" class="form-control" id="inputTime" name="time" min="5" value="5">
    </div>
            </div>
            <input type="submit" value="Generate" class="btn btn-success"><br>
        </form>
        </div>
<?php require APPROOT . '/views/includes/footer.php'; ?>
    </body>
</html>

