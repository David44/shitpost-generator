<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
    <div class="container">
        <a class="navbar-brand" href="<?php echo URLROOT; ?>"><?php echo SITENAME; ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo URLROOT; ?>">Home</a>
                </li>
            </ul>

            <ul class="navbar-nav ml-auto">

                <?php if (isset($_SESSION['user_id'])) : ?>

                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/users/editUser"><?php echo $_SESSION['user_username'] ?></a>
                    </li>

                    <?php if (isset($_SESSION['user_id']) && isset($_SESSION['user_userlevel']) && $_SESSION['user_userlevel'] == adminLevel): ?>


                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/users/manageUsers">Manage Users</a>
                    </li>

                    <?php endif; ?>

                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/users/logout">Logout</a>
                    </li>

                <?php else : ?>

                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/users/register">Register</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-primary text-white btn-block" href="<?php echo URLROOT; ?>/users/login">Login</a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>