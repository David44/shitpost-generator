<?php require APPROOT . '/views/includes/header.php'; ?>

  <a href="<?php echo URLROOT; ?>/emojis/index/1" class="btn btn-info">Back</a>

  <div class="card card-body mt-4">

    <h1><?php echo $data['title']; ?></h1>

    <form action="<?php echo URLROOT; ?>/emojis/generate" method="post">

      <div class="form-group">
          <label for="textToEmojify">Enter a text:</label>
          <input type="text" class="form-control form-control-lg <?php echo (!empty($data['emojiText_error'])) ? 'is-invalid' : ''; ?>" name="textToEmojify" placeholder=" Cram leafs up my..." value="<?php echo $data['textToEmojify']; ?>">
          <span class="invalid-feedback"><?php echo $data['emojiText_error']; ?></span>
      </div>


      <input type="submit" class="btn btn-success" value="Generate">

    </form>

    <br>

    <?php if (!empty($data['generatedEmojifiedText'])) : ?>

    <div class="card">
    <div class="card-body">
      <h4>Generated Emojified Text:</h4>

      <h2 style="display:inline;"><?php echo $data['generatedEmojifiedText']; ?></h2>
      <div class="btn btn-info pull-right" id="btnCopy" data-clipboard-text="<?php echo $data['generatedEmojifiedText']; ?>"><i class="fa fa-files-o" aria-hidden="true"></i> Copy Emojified Text
      </div>


      <?php if (isLoggedIn()) : ?>

      <br>
      <br>

      <form action="<?php echo URLROOT;?>/emojis/create" method="post">
        <input type="hidden" name="emojifiedText" value="<?php echo $data['generatedEmojifiedText']; ?>">
        <input type="submit" value="Post to main feed" class="btn btn-primary">
      </form>

      <?php endif; ?> 

      </div>
      </div>

    <?php endif; ?>


  </div>


<?php require APPROOT . '/views/includes/footer.php'; ?>

<script src="<?php echo URLROOT; ?>/public/js/Emoji/main.js"></script>
<script type="text/javascript" src="<?php echo URLROOT; ?>/public/js/Emoji/clipboard.min.js"></script>
<script type="text/javascript" src="<?php echo URLROOT; ?>/public/js/Emoji/copy.js"></script>