<?php require APPROOT . '/views/includes/header.php'; ?>



<a href="<?php echo URLROOT; ?>/emojis/show/<?php echo $data['id']; ?>" class="btn btn-info">Back</a>

<div class="card card-body mt-4">

  <h1>Edit emojified text</h1>

    <form action="<?php echo URLROOT;?>/emojis/edit/<?php echo $data['id']; ?>" method="post">

      <div class="form-group">
        <label for="emojifiedText">Emojified text title: <sup>*</sup></label>        
        <input type="text" name="emojifiedText" class="form-control form-control-lg <?php echo (!empty($data['emojifiedText_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['emojifiedText']; ?>">
        <span class="invalid-feedback"><?php echo $data['emojifiedText_error']; ?></span>
      </div>

      <input type="submit" class="btn btn-success" value="Update Post">

    </form>
</div>




<?php require APPROOT . '/views/includes/footer.php'; ?>
