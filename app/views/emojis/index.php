<?php require APPROOT . '/views/includes/header.php'; ?>

<?php flash('emojifiedText_message'); ?>
<h1><?php echo $data['title']; ?></h1>

<?php if (isLoggedIn()) : ?>

    <a class="btn btn-success text-white" href="<?php echo URLROOT; ?>/emojis/create">Create</a>

<?php endif; ?>

    <div class="pull-right">
            <a class="btn btn-dark" href="<?php echo URLROOT; ?>/emojis/search"><i class="fa fa-search"></i> Search</a>
        <?php if (isLoggedIn()) : ?>
            <a class="btn btn-dark" href="<?php echo URLROOT; ?>/emojis/showMyEmojifiedTexts">My emojified texts</a>
        <?php endif; ?>   
    </div>



    <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/emojis/generate">Generate</a>

<br/>
<br/>
	
    <?php foreach ($data['emojifiedTexts'] as $emojifiedText) : ?>
    <div class="card">
        <div class="card-body">
        <h6 class="card-subtitle mb-2 text-muted">Submitted by <a href="<?php echo URLROOT; ?>/users/show/<?php echo $emojifiedText['user_id']; ?>"><strong><?php echo $emojifiedText['username'];?></strong></a> at <?php echo $emojifiedText['emojifiedTextCreated']; ?></h6>
            <?php  echo $emojifiedText['emojifiedText']; ?>

                <div class="emojiVote">
                <?php if (isLoggedIn()) : ?> 
                    <?php if (checkIfuserLikedEmojifiedText($_SESSION['user_id'], $emojifiedText['emojifiedText_id']) == 1 ): ?>
                        <span class="unlike fa fa-heart" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $emojifiedText['emojifiedText_id']; ?>"></span> 
                        <span class="like hide fa fa-heart-o" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $emojifiedText['emojifiedText_id']; ?>"></span> 
                    <?php elseif (checkIfuserLikedEmojifiedText($_SESSION['user_id'], $emojifiedText['emojifiedText_id']) == 0): ?>
                    <!-- user has not yet liked post -->
                        <span class="like fa fa-heart-o" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $emojifiedText['emojifiedText_id']; ?>"></span> 
                        <span class="unlike hide fa fa-heart" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $emojifiedText['emojifiedText_id']; ?>"></span> 
                    <?php endif ?>
                    
                    <?php endif; ?>
                    <span class="likes_count"><?php echo $emojifiedText['likes']; ?> likes</span>
                </div>


            <div class="pull-right">               
                <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/emojis/show/<?php echo $emojifiedText['emojifiedText_id'];?>">Show</a>

                <?php if(isset($_SESSION['user_id']) && $emojifiedText['user_id'] == $_SESSION['user_id']) : ?>
                    <a class="btn btn-success" href="<?php echo URLROOT; ?>/emojis/edit/<?php echo $emojifiedText['emojifiedText_id'];?>">Edit</a> 
                    <form class="pull-right" action="<?php echo URLROOT;?>/emojis/delete/<?php echo $emojifiedText['emojifiedText_id'];?>" method="post">
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                <?php endif; ?>

            </div>
        </div>       
    </div>

    <?php endforeach; ?>

    <br>

    <ul class="pagination">
        <?php for($i = 1; $i <= $data['pages']; $i++): ?>
            <li class="page-item <?php if ($data['curentPage'] === $i) { echo ' active';} ?>"><a class="page-link" href="<?php echo URLROOT; ?>/emojis/index/<?php echo $i;?>"><?php echo $i; ?></a></li>
        <?php endfor; ?>
    </ul>

    

<?php require APPROOT . '/views/includes/footer.php'; ?>
<script src="<?php echo URLROOT; ?>/public/js/Emoji/main.js"></script>