<?php require APPROOT . '/views/includes/header.php'; ?>



<a href="<?php echo URLROOT; ?>/emojis/index/1" class="btn btn-info">Back</a>

<div class="card card-body mt-4">

  <h1><?php echo $data['title']; ?></h1>

  <h3>Post your own emojified text</h3>

    <form action="<?php echo URLROOT;?>/emojis/create" method="post">

      <div class="form-group">
        <label for="emojifiedText">Text to emojify: <sup>*</sup></label>        
        <input type="text" name="emojifiedText" class="form-control form-control-lg <?php echo (!empty($data['emojifiedText_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['emojifiedText']; ?>">
        <span class="invalid-feedback"><?php echo $data['emojifiedText_error']; ?></span>
      </div>

      <input type="submit" class="btn btn-success" value="Post">

    </form>
</div>




<?php require APPROOT . '/views/includes/footer.php'; ?>
