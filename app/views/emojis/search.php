<?php require APPROOT . '/views/includes/header.php'; ?>



<a href="<?php echo URLROOT; ?>/emojis/index/1" class="btn btn-info">Back</a>

<div class="card card-body mt-4">

  <h3>Search emojified text by keywoard</h3>

    <form action="<?php echo URLROOT;?>/emojis/search" method="post">

      <div class="form-group">
        <label for="emojifiedTextSearch">Search text: </label>        
        <input type="text" name="emojifiedTextSearch" class="form-control form-control-lg <?php echo (!empty($data['emojifiedTextSearch_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['emojifiedTextSearch']; ?>">
        <span class="invalid-feedback"><?php echo $data['emojifiedTextSearch_error']; ?></span>
      </div>

      <input type="submit" class="btn btn-primary" value="Search">

    </form>

    <br>
    <?php if (empty($data['emojifiedTexts'])) : ?>
      No results.

    <?php else : ?> 

      <div class="card">

      <h4 class="card-title">
        Found results with <span class="text-success"><?php echo $data['emojifiedTextSearch']; ?></span> keyword:
      </h4>
      </div>
      <br>

      <?php foreach ($data['emojifiedTexts'] as $emojifiedText) : ?>
      
      <div class="card">
          <div class="card-body">
          <h6 class="card-subtitle mb-2 text-muted">Created at <?php echo $emojifiedText['created_at']; ?></h6>
              <?php echo $emojifiedText['emojifiedText']; ?>

              <div class="pull-right">
                  <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/emojis/show/<?php echo $emojifiedText['id'];?>">Show</a>
              </div>
          </div>       
      </div>

      <br>

      <?php endforeach; ?>

    <?php endif; ?>

</div>

<br>


<?php require APPROOT . '/views/includes/footer.php'; ?>
