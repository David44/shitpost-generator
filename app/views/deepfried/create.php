<style>
    img{
        Max-width: 100%;
        height:auto;
    }
</style>

<?php require APPROOT . '/views/includes/header.php'; ?>

<a href="<?php echo URLROOT; ?>/deepfried/index/1" class="btn btn-info">Back</a>

<div class="card card-body mt-4">

  <h1><?php echo $data['title']; ?></h1>

  <h3>Upload your image</h3>

    <form action="<?php echo URLROOT;?>/deepfried/create" method="post" enctype="multipart/form-data">

      <div class="form-group">
          <input type="file" accept="image/*" onchange="preview_image(event)" name="upfile" id="upfile" />
          <br><br>
          <img id="output_image"/>
      </div>

      <input type="submit" class="btn btn-success" value="Post">

    </form>
</div>


<?php require APPROOT . '/views/includes/footer.php'; ?>

<script type="text/javascript" src="<?php echo URLROOT; ?>/public/js/Deepfry/upload.js"></script>