<style>
    img{
        Max-width: 100%;
        height:auto;
    }
</style>

<?php require APPROOT . '/views/includes/header.php'; ?>

<a href="<?php echo URLROOT; ?>/deepfried/index/1" class="btn btn-info">Back</a>

<?php if(isset($_SESSION['user_id']) && $data['deepfried']['user_id'] == $_SESSION['user_id']) : ?>
  <div class="pull-right">
      <form class="pull-right" action="<?php echo URLROOT;?>/deepfried/delete/<?php echo $data['deepfried']['id'];?>" method="post">
      <input type="submit" value="Delete Image" class="btn btn-danger">
    </form>

  </div>
<?php endif; ?>

<br>

<div class="card mb-3">

  <div class="card-body">

      <img src=<?php echo URLROOT."/public/img/".$data['deepfried']['path']; ?>><br><br>

    <em>Submitted by <a href="<?php echo URLROOT; ?>/users/show/<?php echo $data['user']['id']; ?>"><strong><?php echo $data['user']['username']; ?></strong></a> on <?php echo $data['deepfried']['created_at'];?></em>
    <em><strong>Likes: <?php echo $data['deepfried']['likes']; ?></strong></em>



  </div>
</div>

<h3>Comments</h3>

<?php flash('deepfried_comment_message'); ?>

<?php if (!empty($data['comments'])) : ?>

  <?php foreach ($data['comments'] as $comment) : ?>
      
  <div class="card">
      <div class="card-body">
          <a href="<?php echo URLROOT; ?>/users/show/<?php echo $comment['userId'];?>"><strong><?php echo $comment['username'];?></strong></a>(<?php echo $comment['commentCreated']; ?>)
          <br>
          <?php echo $comment['comment']; ?>

          <?php if(isset($_SESSION['user_id']) && $comment['userId'] == $_SESSION['user_id']) : ?>

            <form class="pull-right" action="<?php echo URLROOT;?>/deepfried/deleteComment/<?php echo $comment['commentId'];?>/deepfried/<?php echo $data['deepfried']['id']; ?>" method="post">
              <input type="submit" value="Remove" class="btn btn-danger">
            </form>

        <?php endif; ?>
      </div>       
  </div>

  <?php endforeach; ?>

<?php else : ?>
  <p>Currently there are no comments.</p>
<?php endif; ?>
<br>
<br>

<?php if (isLoggedIn()) : ?>

  <div class="card">
      <div class="card-body">
      <h3>Post your comment</h3>

      <form action="<?php echo URLROOT;?>/deepfried/createComment/<?php echo $data['deepfried']['id'];?>" method="post">

        <div class="form-group">
          <label for="comment">Comment: <sup>*</sup></label>        
          <input type="text" name="comment" class="form-control form-control-lg <?php echo (!empty($data['comment_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['comment']; ?>" required>
          <span class="invalid-feedback"><?php echo $data['comment_error']; ?></span>
        </div>

        <input type="submit" class="btn btn-success" value="Comment">

      </form>
      </div>       
  </div>

  <br>

<?php endif; ?>
    
<?php require APPROOT . '/views/includes/footer.php'; ?>
