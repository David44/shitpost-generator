<?php require APPROOT . '/views/includes/header.php'; ?>

<div class="row">
    <div class="col-md-4 mx-auto">
        <div class="card card-body bg-light mt-4">

            <?php flash('editUser_success'); ?>

            <h2>Edit user account</h2>

            <form action="<?php echo URLROOT;?>/users/editUser" method="post">
                <div class="form-group">
                    <label for="name">Username: </label>
                    <input type="text" name="name" class="form-control form-control-lg <?php echo (!empty($data['name_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['name']; ?>">
                    <span class="invalid-feedback"><?php echo $data['name_error']; ?></span>
                </div>

                <div class="form-group">
                    <label for="email">Email: </label>
                    <input type="email" name="email" class="form-control form-control-lg <?php echo (!empty($data['email_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['email']; ?>">
                    <span class="invalid-feedback"><?php echo $data['email_error']; ?></span>
                </div>

                <div class="form-group">
                    <label for="old_password">Old password: </label>
                    <input type="password" name="old_password" class="form-control form-control-lg <?php echo (!empty($data['old_password_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['old_password']; ?>">
                    <span class="invalid-feedback"><?php echo $data['old_password_error']; ?></span>
                </div>

                <div class="form-group">
                    <label for="password">New password: </label>
                    <input type="password" name="password" class="form-control form-control-lg <?php echo (!empty($data['password_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['password']; ?>">
                    <span class="invalid-feedback"><?php echo $data['password_error']; ?></span>
                </div>

                <div class="form-group">
                    <label for="confirm_password">New password confirm: </label>
                    <input type="password" name="confirm_password" class="form-control form-control-lg <?php echo (!empty($data['confirm_password_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['confirm_password']; ?>">
                    <span class="invalid-feedback"><?php echo $data['confirm_password_error']; ?></span>
                </div>

                <input type="submit" value="Update" class="btn btn-primary btn-block">

            </form>
        </div>
    </div>
</div>

<?php require APPROOT . '/views/includes/footer.php'; ?>
