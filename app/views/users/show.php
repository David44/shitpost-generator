<?php require APPROOT . '/views/includes/header.php'; ?>

<a href="<?php echo URLROOT; ?>/pages" class="btn btn-info">Back</a>


<div class="card mb-3">

  <div class="card-body">
    <h1>User profile info</h1>
    Username: <strong><?php echo $data['user']['username']; ?></strong><br>
    Email: <strong><?php echo $data['user']['email']; ?></strong><br>
    User-level: <strong><?php echo $data['user']['user_level']; ?></strong><br>
    Status: <strong><?php echo $data['user']['status']; ?></strong>

  </div>
</div>

    
<?php require APPROOT . '/views/includes/footer.php'; ?>
