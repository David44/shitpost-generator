<?php require APPROOT . '/views/includes/header.php'; ?>



    <br>
    <?php foreach ($data['users'] as $user) : ?>
    <div class="card">
        <div class="card-body">


            <strong>ID</strong>:<?php echo $user['id']; ?>

            <strong>USERNAME</strong>:<?php echo $user['username']; ?>

            <strong>EMAIL</strong>:<?php echo $user['email']; ?>

            
            <strong>USER LEVEL</strong>:<?php echo $user['user_level']; ?>

            <?php if ($user['status'] == 'active') : ?>       
                <strong >STATUS</strong>:<span class="text-success"><?php echo $user['status']; ?></span>
            <?php else : ?>
                <strong >STATUS</strong>:<span class="text-danger"><?php echo $user['status']; ?></span>
            <?php endif; ?>

            <div class="pull-right">
                <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/users/show/<?php echo $user['id'];?>">Show</a>
            

            <?php if ($user['status'] == 'active' && $_SESSION['user_id'] != $user['id']) : ?>
                <a class="btn btn-danger" href="<?php echo URLROOT; ?>/users/banUser/<?php echo $user['id'];?>">Ban user</a>

            <?php elseif ($user['status'] == 'banned' && $_SESSION['user_id'] != $user['id']) :?>
                <a class="btn btn-success" href="<?php echo URLROOT; ?>/users/unbanUser/<?php echo $user['id'];?>">Unban user</a>
            <?php endif; ?>

            </div>
        
        </div>       
    </div>

    <?php endforeach; ?>

    <br>


<?php require APPROOT . '/views/includes/footer.php'; ?>
