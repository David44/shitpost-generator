<?php require APPROOT . '/views/includes/header.php'; ?>



<a href="<?php echo URLROOT; ?>/clickbaits/index/1" class="btn btn-info">Back</a>

<div class="card card-body mt-4">

  <h3>Search clickbait by keywoard</h3>

    <form action="<?php echo URLROOT;?>/clickbaits/search" method="post">

      <div class="form-group">
        <label for="clickbaitSearch">Search text: </label>        
        <input type="text" name="clickbaitSearch" class="form-control form-control-lg <?php echo (!empty($data['clickbaitSearch_error'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['clickbaitSearch']; ?>">
        <span class="invalid-feedback"><?php echo $data['clickbaitSearch_error']; ?></span>
      </div>

      <input type="submit" class="btn btn-primary" value="Search">

    </form>

    <br>
    <?php if (empty($data['clickbaits'])) : ?>
      No results.

    <?php else : ?> 

      <div class="card">

      <h4 class="card-title">
        Found results with <span class="text-success"><?php echo $data['clickbaitSearch']; ?></span> keyword:
      </h4>
      </div>
      <br>

      <?php foreach ($data['clickbaits'] as $clickbait) : ?>
      
      <div class="card">
          <div class="card-body">
          <h6 class="card-subtitle mb-2 text-muted">Created at <?php echo $clickbait['created_at']; ?></h6>
              <?php echo $clickbait['clickbait']; ?>

              <div class="pull-right">
                  <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/clickbaits/show/<?php echo $clickbait['id'];?>">Show</a>
              </div>
          </div>       
      </div>

      <br>

      <?php endforeach; ?>

    <?php endif; ?>

</div>

<br>


<?php require APPROOT . '/views/includes/footer.php'; ?>
