<?php require APPROOT . '/views/includes/header.php'; ?>

  <a href="<?php echo URLROOT; ?>/clickbaits/index/1" class="btn btn-info">Back</a>

  <div class="card card-body mt-4">

    <h1><?php echo $data['title']; ?></h1>

    <form action="<?php echo URLROOT; ?>/clickbaits/generate" method="post">

      <div class="form-group">
          <label for="clickbaitWord">Enter a word:</label>
          <input type="text" class="form-control form-control-lg <?php echo (!empty($data['clickbait_error'])) ? 'is-invalid' : ''; ?>" name="clickbaitWord" placeholder="Ex. cat" value="<?php echo $data['clickbaitWord']; ?>">
          <span class="invalid-feedback"><?php echo $data['clickbait_error']; ?></span>
      </div>

      <fieldset class="form-group">
          <legend>Choose word form</legend>
          <div class="form-check">
            <label class="form-check-label">
              <input type="radio" onclick="javascript:clickbaitOptionsCheck();" class="form-check-input" name="optionsRadios" id="nounCheck" value="nounCheck" checked>
              Noun
            </label>
          </div>
          <div class="form-check">
          <label class="form-check-label">
              <input type="radio" onclick="javascript:clickbaitOptionsCheck();" class="form-check-input" name="optionsRadios" id="verbCheck" value="verbCheck">
              Adjective
            </label>
          </div>    
      </fieldset>

        <div id="ifYes" style="display:block;">
          <fieldset class="form-group">
              <legend>Is noun single or plural?</legend>
              <div class="form-check">
                <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="ExtraOptionsRadios" id="single" value="single" <?php if ($data['singleOrPluralNoun'] == 'single') { echo "checked"; } ?>>
                  Single
                </label>
              </div>
              <div class="form-check">
              <label class="form-check-label">
                  <input type="radio" class="form-check-input" name="ExtraOptionsRadios" id="plural" value="plural" <?php if ($data['singleOrPluralNoun'] == 'plural') { echo "checked"; } ?>>
                  Plural
                </label>
              </div>    
          </fieldset>
        </div>

      <input type="submit" class="btn btn-success" value="Generate">

    </form>

    <br>

    <?php if (!empty($data['generatedClickbait'])) : ?>

    <div class="card">
    <div class="card-body">
      <h4>Generated Clickbait:</h4>

      <h2 style="display:inline;"><?php echo $data['generatedClickbait']; ?></h2>
      <div class="btn btn-info pull-right" id="btnCopy" data-clipboard-text="<?php echo $data['generatedClickbait']; ?>"><i class="fa fa-files-o" aria-hidden="true"></i> Copy Clickbait
      </div>


      <?php if (isLoggedIn()) : ?>

      <br>
      <br>

      <form action="<?php echo URLROOT;?>/clickbaits/create" method="post">
        <input type="hidden" name="clickbait" value="<?php echo $data['generatedClickbait']; ?>">
        <input type="submit" value="Post to main feed" class="btn btn-primary">
      </form>

      <?php endif; ?> 

      </div>
      </div>

    <?php endif; ?>


  </div>


<?php require APPROOT . '/views/includes/footer.php'; ?>

<script src="<?php echo URLROOT; ?>/public/js/Clickbait/main.js"></script>
<script type="text/javascript" src="<?php echo URLROOT; ?>/public/js/Clickbait/clipboard.min.js"></script>
<script type="text/javascript" src="<?php echo URLROOT; ?>/public/js/Clickbait/copy.js"></script>