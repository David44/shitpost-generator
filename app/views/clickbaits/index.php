<?php require APPROOT . '/views/includes/header.php'; ?>

<?php flash('clickbait_message'); ?>

<h1><?php echo $data['title']; ?></h1>

<?php if (isLoggedIn()) : ?>

    <a class="btn btn-success text-white" href="<?php echo URLROOT; ?>/clickbaits/create">Create</a>

<?php endif; ?>

    <div class="pull-right">
            <a class="btn btn-dark" href="<?php echo URLROOT; ?>/clickbaits/search"><i class="fa fa-search"></i> Search</a>
        <?php if (isLoggedIn()) : ?>
            <a class="btn btn-dark" href="<?php echo URLROOT; ?>/clickbaits/showMyClickbaits">My clickbaits</a>
        <?php endif; ?>   
    </div>



    <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/clickbaits/generate">Generate</a>

<br/>
<br/>

    <?php foreach ($data['clickbaits'] as $clickbait) : ?>
    <div class="card">
        <div class="card-body">
        <h6 class="card-subtitle mb-2 text-muted">Submitted by <a href="<?php echo URLROOT; ?>/users/show/<?php echo $clickbait['userId']; ?>"><strong><?php echo $clickbait['username'];?></strong></a> at <?php echo $clickbait['clickbaitCreated']; ?></h6>
            <?php echo $clickbait['clickbait']; ?>

                <div class="clickbaitVote">
                <?php if (isLoggedIn()) : ?> 
                    <?php if (checkIfuserLikedClickbait($_SESSION['user_id'], $clickbait['clickbaitId']) == 1 ): ?>
                        <span class="unlike fa fa-heart" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $clickbait['clickbaitId']; ?>"></span> 
                        <span class="like hide fa fa-heart-o" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $clickbait['clickbaitId']; ?>"></span> 
                    <?php elseif (checkIfuserLikedClickbait($_SESSION['user_id'], $clickbait['clickbaitId']) == 0): ?>
                    <!-- user has not yet liked post -->
                        <span class="like fa fa-heart-o" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $clickbait['clickbaitId']; ?>"></span> 
                        <span class="unlike hide fa fa-heart" data-user="<?php echo $_SESSION['user_id']; ?>" data-id="<?php echo $clickbait['clickbaitId']; ?>"></span> 
                    <?php endif ?>
                    
                    <?php endif; ?>
                    <span class="likes_count"><?php echo $clickbait['likes']; ?> likes</span>
                </div>


            <div class="pull-right">               
                <a class="btn btn-primary text-white" href="<?php echo URLROOT; ?>/clickbaits/show/<?php echo $clickbait['clickbaitId'];?>">Show</a>

                <?php if(isset($_SESSION['user_id']) && $clickbait['user_id'] == $_SESSION['user_id']) : ?>
                    <a class="btn btn-success" href="<?php echo URLROOT; ?>/clickbaits/edit/<?php echo $clickbait['clickbaitId'];?>">Edit</a> 
                    <form class="pull-right" action="<?php echo URLROOT;?>/clickbaits/delete/<?php echo $clickbait['clickbaitId'];?>" method="post">
                        <input type="submit" value="Delete" class="btn btn-danger">
                    </form>
                <?php endif; ?>

            </div>
        </div>       
    </div>

    <?php endforeach; ?>

    <br>

    <ul class="pagination">
        <?php for($i = 1; $i <= $data['pages']; $i++): ?>
            <li class="page-item <?php if ($data['curentPage'] === $i) { echo ' active';} ?>"><a class="page-link" href="<?php echo URLROOT; ?>/clickbaits/index/<?php echo $i;?>"><?php echo $i; ?></a></li>
        <?php endfor; ?>
    </ul>

    

<?php require APPROOT . '/views/includes/footer.php'; ?>
<script src="<?php echo URLROOT; ?>/public/js/Clickbait/main.js"></script>