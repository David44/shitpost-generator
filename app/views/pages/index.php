<?php require APPROOT . '/views/includes/header.php'; ?>

    <div class="text-center">
        <h1><?php echo $data['title']; ?></h1>

        <h1 class="big-logo"><?php echo $data['logo']; ?></h1>
    </div>

    <br/>
    <div class="row">
        <div class="col">
            <a href="<?php echo URLROOT; ?>/clickbaits/index/1" class="btn btn-primary btn-lg btn-block">Clickbaits</a>
        </div>
        <div class="col">
            <a href="<?php echo URLROOT; ?>/candle/index" class="btn btn-primary btn-lg btn-block">Candle</a>
        </div>
        <div class="col">
            <a href="<?php echo URLROOT; ?>/emojis/index/1" class="btn btn-primary btn-lg btn-block">Emojis</a>
        </div>
        <div class="col">
            <a href="<?php echo URLROOT; ?>/deepfried/index/1" class="btn btn-primary btn-lg btn-block">Images</a>
        </div>
    </div>

    <br>

    </div>
    <section class="bg-dark text-white">
      <div class="container text-center">
        <?php if (isLoggedIn()) : ?>
            <h2>Thank you for using our website!</h2>
            <h3 class="mb-4">You can now share, like, save and comment !</h3>
        <?php else : ?> 
            <h2 class="mb-4">Register to share, like, comment and save your created content!</h2>
            <a class="btn btn-primary btn-lg mb-4" href="<?php echo URLROOT; ?>/users/register">SIGN UP</a>
        <?php endif; ?>

      </div>
    </section>

    <div class="container">

        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="mt-5 mx-auto">
              <i class="fa fa-newspaper-o fa-4x text-primary mb-3"></i>
              <h3 class="mb-3">Create Clickbaits!</h3>
              <p class="text-muted mb-0">Create and generate random clickbait titles</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="mt-5 mx-auto">
              <i class="fa fa-bolt fa-4x text-primary mb-3"></i>
              <h3 class="mb-3">Generate candles!</h3>
              <p class="text-muted mb-0">You can create candles with various parameters</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="mt-5 mx-auto">
              <i class="fa fa-smile-o fa-4x text-primary mb-3"></i>
              <h3 class="mb-3">Emojify your text!</h3>
              <p class="text-muted mb-0">Add emoji to your inserted text</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="mt-5 mx-auto">
              <i class="fa fa-4x fa-file-image-o text-primary mb-3"></i>
              <h3 class="mb-3">Edit your images!</h3>
              <p class="text-muted mb-0">Edit, add JPEG articafacts, liquify and more!</p>
            </div>
          </div>
        </div>

<?php require APPROOT . '/views/includes/footer.php'; ?>
