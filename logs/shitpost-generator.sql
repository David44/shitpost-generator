-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2018 m. Sau 10 d. 10:34
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shitpost-generator`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `clickbaits`
--

CREATE TABLE `clickbaits` (
  `id` int(11) NOT NULL,
  `clickbait` varchar(255) CHARACTER SET utf8 NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `likes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sukurta duomenų kopija lentelei `clickbaits`
--

INSERT INTO `clickbaits` (`id`, `clickbait`, `user_id`, `created_at`, `likes`) VALUES
(1, '10 facts about cats that will shock you', 9, '2017-12-24 17:35:09', 1),
(2, 'Crazy apple eating methods\r\n', 10, '2017-12-24 17:35:09', 1),
(3, 'These facts about Christmas will schock you!', 9, '2017-12-24 20:10:00', 1),
(5, '123456sdfsdfsfsdfsdf2', 11, '2017-12-24 20:31:13', 0),
(10, 'asdasdasdasdas', 11, '2017-12-26 18:29:48', 1),
(13, 'ljkjljkljkljkljkljkljkljkl', 11, '2017-12-26 19:57:33', 0),
(14, 'llkkllklkklklklkl', 9, '2017-12-26 19:58:07', 0),
(15, 'asdasdasdasdasdzxczxczxczxc', 11, '2017-12-26 20:00:31', 0),
(16, 'asdasdasdqweqweqweqweqweadasdasdasdhjkhjkhj', 11, '2017-12-26 20:01:13', 0),
(17, 'qeqweqweqweqweqweqw', 11, '2017-12-26 20:03:45', 0),
(18, 'dasdasqweqweqweqweqwe', 9, '2017-12-26 20:04:55', 0),
(22, 'qweqweqweqweqweqwe', 9, '2017-12-26 20:09:00', 1),
(23, 'dasdadwqeqweqweqweqweqweqweqwe123123123', 9, '2017-12-26 20:09:11', 1),
(26, 'qweqweqweqweqweqw', 9, '2017-12-26 20:46:17', 3),
(27, 'rwerwerwerwerwesdfsdfsdf12356', 11, '2017-12-26 21:07:34', 1),
(28, 'zxczxczxczxc', 11, '2017-12-26 21:09:59', 1),
(29, 'dog clickbait', 12, '2017-12-28 20:19:29', 0),
(30, 'What We Found about cats Was Shocking', 11, '2018-01-02 20:14:59', 1),
(32, 'Youâ€™ll Never Guess Why dogs are brave', 11, '2018-01-06 17:46:53', 1),
(33, 'When you read These 81 intense cats facts. Youâ€™ll Never Want to eat again', 11, '2018-01-06 20:11:38', 1),
(34, 'testing bait nr 2.', 11, '2018-01-07 18:17:25', 1),
(35, '41 Tips That Will Keep You From Becoming a cat', 11, '2018-01-07 18:18:14', 1),
(37, 'asdasdasdasd', 10, '2018-01-07 20:36:54', 2);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `clickbaits_comments`
--

CREATE TABLE `clickbaits_comments` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `clickbait_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `clickbaits_comments`
--

INSERT INTO `clickbaits_comments` (`id`, `comment`, `clickbait_id`, `user_id`, `created_at`) VALUES
(12, 'asdasdasdasd', 16, 11, '2017-12-26 20:03:29'),
(13, 'zxczxczxczxc', 16, 11, '2017-12-26 20:03:32'),
(14, 'qeqweqweqwe', 23, 9, '2017-12-26 20:25:43'),
(15, 'ssadadadasd', 23, 9, '2017-12-26 20:25:46'),
(16, 'dam son.', 33, 11, '2018-01-06 20:11:48'),
(17, 'this is a test comment.', 34, 11, '2018-01-07 18:17:41');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `clickbait_likes`
--

CREATE TABLE `clickbait_likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `clickbait_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `clickbait_likes`
--

INSERT INTO `clickbait_likes` (`id`, `user_id`, `clickbait_id`) VALUES
(744, 11, 10),
(745, 11, 2),
(746, 9, 26),
(747, 9, 22),
(748, 11, 23),
(758, 11, 26),
(766, 11, 3),
(767, 12, 28),
(768, 12, 27),
(769, 12, 26),
(770, 12, 1),
(771, 11, 30),
(832, 11, 33),
(833, 11, 32),
(860, 11, 39),
(866, 11, 34),
(867, 11, 46),
(868, 15, 46),
(869, 14, 53),
(870, 15, 47),
(871, 15, 62),
(872, 15, 63),
(873, 15, 64),
(874, 15, 65),
(875, 15, 66),
(876, 15, 76),
(877, 16, 83),
(883, 11, 35),
(904, 11, 37),
(905, 10, 37);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `deepfried`
--

CREATE TABLE `deepfried` (
  `id` int(11) NOT NULL,
  `path` varchar(255) CHARACTER SET utf8 NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `likes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sukurta duomenų kopija lentelei `deepfried`
--

INSERT INTO `deepfried` (`id`, `path`, `user_id`, `created_at`, `likes`) VALUES
(3, '98a169ccba2e1a1a486da0271f19c3fa1e2bba74.png', 14, '2018-01-07 02:42:10', 1),
(4, 'bfc85c9f8b32226fcc79f30e09c981bf0d1f68fa.png', 14, '2018-01-07 02:57:13', 2),
(9, '5500bcdd91c2329024b77f7427b475fce44aaf64.jpg', 14, '2018-01-07 04:44:15', 0),
(13, '7e26db8b29a5b43eb5b26ba64692ff78a05ab7d2.jpg', 14, '2018-01-07 06:35:06', 0),
(14, '032a3a9198312eb1aaa9492c7eade66636c222c8.png', 14, '2018-01-07 06:36:24', 0),
(16, 'ce4964664d031ce3b22e1dfa5eafbc6b0c02a3e5.jpg', 14, '2018-01-07 06:37:35', 0),
(17, 'aa7b73397f09f2909d21d92ce4a37a7f25c56517.jpg', 14, '2018-01-07 06:37:48', 0),
(18, '1261eefcc6994a72dd01b7fb5aad21899df1e026.png', 14, '2018-01-07 06:39:58', 0),
(29, 'cfdce3ff6d725fc860aa72e2342b25bffe23276a.png', 14, '2018-01-07 07:28:29', 0),
(31, '2181f0384c5a44ae2b9d5c10f6ef0f9c53f829de.png', 13, '2018-01-07 07:34:22', 0),
(32, 'fc3cce172d42179156f0cf8885f1e054243859f7.jpg', 13, '2018-01-07 07:37:50', 0),
(33, '633e35479f67f8ac55f4c6207b6b468b300a3260.png', 14, '2018-01-07 07:40:42', 0),
(36, 'faa25f96767470bfc050e983b24a5d0f41eaeec4.png', 14, '2018-01-07 17:08:20', 2),
(38, '5155237548c534f7b74240f0572907248cae24db.jpg', 11, '2018-01-07 17:52:44', 0),
(39, '41087ab733d0ed49c34f855c2e1013e1d63b7492.png', 14, '2018-01-07 17:52:46', 1),
(40, '60c8996ab700c15b0e2ec9cb27a1807e96236da9.jpg', 15, '2018-01-07 18:41:27', 0),
(41, '805cb8112e5e2d5f481b76e6c8e38981d94e1fe4.png', 15, '2018-01-07 19:02:14', 0),
(42, 'a3c1febf2969c78a7fd1118e5af5f8d5178bd362.jpg', 16, '2018-01-07 19:10:47', 0),
(43, 'ff420186647983091cfb9303420fda4d73ebd6f2.png', 16, '2018-01-07 19:16:53', 0),
(44, '1a749689ed367649b7933f0c2c411c866de909e0.jpg', 16, '2018-01-07 19:17:37', 0),
(45, '91e1ee188cdf700632115d62221f3eab42e28513.jpg', 16, '2018-01-07 19:17:45', 0),
(46, '8bd97659bcf6ca47116eef5112c6912e78e9803e.jpg', 16, '2018-01-07 19:18:27', 2),
(47, 'b0c3426aa8def31d24774b4b84c10681bf8777ec.jpg', 16, '2018-01-07 19:18:51', 1),
(48, 'fbac7ec621d733231e20b123702816f7b0216e44.jpg', 16, '2018-01-07 19:19:03', 0),
(49, '976d6abbc07c990fc52cf40955f909e49b1d1b0e.png', 16, '2018-01-07 19:19:12', 0),
(50, '392b06e246e7757ced1f5695b5dd633c55aabbe9.jpg', 16, '2018-01-07 19:19:22', 0),
(51, '22ae381beb15e89797b1466777a4dc2db3f00973.gif', 16, '2018-01-07 19:19:42', 0),
(52, 'c16cd2a256e9541541d1301a56504c1886cfd4a5.jpg', 16, '2018-01-07 19:20:40', 0),
(53, '01749585e4ace76701c9bcf424b90c3d83bf5521.jpg', 16, '2018-01-07 19:20:58', 1),
(54, '5e7ba64bc00734bdc594109977b5dfb764ffa56f.jpg', 16, '2018-01-07 19:24:51', 0),
(55, 'fb48fe42ec25604dd3d4933762e32d7caeb1998e.jpg', 16, '2018-01-07 19:25:08', 0),
(56, 'a24a59bc60fff3321d739c79c6f93564f256718b.jpg', 16, '2018-01-07 19:25:30', 0),
(57, '2a78fa0e076476e400aaaacd54ab61b7b0981a52.png', 16, '2018-01-07 19:25:39', 0),
(58, 'cf285ceea04f52007dc501cfd9e35b488beb30cf.jpg', 16, '2018-01-07 19:26:09', 0),
(59, '1107da9734756a9dc8b1716d97f511f3852ee1cc.jpg', 16, '2018-01-07 19:27:58', 0),
(61, '4b440e04763987c43be14b58b1ceb607fe4ca177.jpg', 16, '2018-01-07 19:28:25', 0),
(62, '57e0314c1b267d8dd8e948c098c4db9f5d0ccbe9.gif', 16, '2018-01-07 19:29:00', 1),
(63, 'bf951ac8d9ebab4c2bd3092e1006a8d15eb46922.gif', 16, '2018-01-07 19:29:11', 1),
(64, '7a6901decc702180a96248e9a30fdd82ee83e038.gif', 16, '2018-01-07 19:29:23', 1),
(65, 'cad6f44c5928b071d417fd09a9830c4e5725a06b.gif', 16, '2018-01-07 19:29:32', 1),
(66, '7fcd4497736e08aacf32f21525418dfa151ec63c.gif', 16, '2018-01-07 19:29:42', 1),
(67, 'afb3f5cb3d1f18d43a06806cf8f81475d61b234b.gif', 16, '2018-01-07 19:30:04', 1),
(68, 'c4ec7296019fd10c229a409b51876a3fe26222a9.jpg', 16, '2018-01-07 19:31:18', 1),
(69, '905de4d2e6dd7b1abed510f454183a2fc62d2be3.jpg', 16, '2018-01-07 19:32:35', 1),
(70, 'b63cbe600cf5715f11fd343bd614c9e8623fb085.jpg', 16, '2018-01-07 19:33:15', 1),
(71, '619518756d7f3b2f36d3b73cff9aab8d0bab9896.jpg', 16, '2018-01-07 19:33:33', 1),
(72, '4f996fe4abf8da1eb6faf286c97b893dccdee2eb.jpg', 16, '2018-01-07 19:34:25', 1),
(73, 'a6c0be0ab3dae6707690e2d0d41a5b9e2b7f6640.jpg', 16, '2018-01-07 19:36:35', 1),
(74, '4249ed6297cb8659e5a5b256b07db0c779ce1687.jpg', 16, '2018-01-07 19:37:28', 1),
(75, 'e8f3e7ead8c2045a87f33975ddcd81147b8a10bd.jpg', 16, '2018-01-07 19:39:14', 1),
(76, '2df4a4602c965f6619714605a0e9c73eb9ed8097.jpg', 16, '2018-01-07 19:40:27', 2),
(77, '9d1ab17b94644cf03d29b1482086622a114c3773.jpg', 16, '2018-01-07 19:42:46', 1),
(78, 'acec7b791815c68f1c49e762f12d3db6d12174da.png', 16, '2018-01-07 19:43:12', 1),
(79, '4fbb7b7fbc5a3b76fde3ee02cd82a7859f9ff87d.jpg', 16, '2018-01-07 20:04:00', 1),
(80, '2db88bfd7647607a9b3bbe0595a258f8a82a7f4f.jpg', 16, '2018-01-07 20:04:15', 1),
(81, '2687966482d257d5a2d5368f56091e33770b7c86.png', 11, '2018-01-07 20:14:17', 1),
(82, '7ffda2968501869672bae4cc0c23bce7e5009f51.png', 16, '2018-01-07 20:17:02', 1),
(83, '7041c3365fa02e134f5d5d9ef244ee8079c996f3.jpg', 16, '2018-01-07 20:17:55', 1),
(84, 'a6d1e5c91e91ad9101be8addea621c7de9fc54b0.jpg', 14, '2018-01-07 20:31:38', 2),
(85, 'd9646820f36b7e0fa9bb7514845e3e77cc013b0e.png', 16, '2018-01-07 20:37:03', 1),
(86, 'e5f9537482945dd78fa7f38ce56e6b81fdd35f1f.gif', 16, '2018-01-07 20:37:29', 1),
(87, '5e2a42a9d70c55436ac9afcb3634d8536d8a7580.jpg', 16, '2018-01-07 20:46:55', 0);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `deepfried_comments`
--

CREATE TABLE `deepfried_comments` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `deepfry_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sukurta duomenų kopija lentelei `deepfried_comments`
--

INSERT INTO `deepfried_comments` (`id`, `comment`, `deepfry_id`, `user_id`, `created_at`) VALUES
(3, 'XD TOP KEK LMAO', 2, 14, '2018-01-07 03:28:53'),
(4, 'yes', 4, 13, '2018-01-07 04:08:11'),
(5, 'lel!', 21, 14, '2018-01-07 07:08:53'),
(6, 'lel!', 34, 14, '2018-01-07 18:40:39'),
(7, 'damn lmao', 35, 14, '2018-01-07 19:07:56'),
(8, 'nice.', 39, 11, '2018-01-07 17:53:11'),
(9, 'comment', 3, 14, '2018-01-07 18:37:55'),
(10, 'âœ‹ðŸ˜©ðŸ‘Œ', 40, 14, '2018-01-07 18:45:46'),
(11, 'BRUH', 41, 14, '2018-01-07 19:12:04'),
(12, 'faster', 62, 15, '2018-01-07 19:37:07'),
(13, 'Faster', 63, 15, '2018-01-07 19:37:21'),
(14, 'FASTER', 64, 15, '2018-01-07 19:37:48'),
(16, 'FASTER!!!!Ä„Ä„!!', 65, 15, '2018-01-07 19:38:10'),
(17, 'AAAAAA F AAAAAA A AAAAAA S AAAAAAA T AAAAAA E AAAAAA R AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA', 66, 15, '2018-01-07 19:38:55'),
(18, 'Me irl xd', 76, 15, '2018-01-07 19:42:45'),
(19, 'ðŸ˜ŽðŸ˜ŽðŸ˜Ž', 81, 14, '2018-01-07 20:17:47');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `deepfried_likes`
--

CREATE TABLE `deepfried_likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `deepfry_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Sukurta duomenų kopija lentelei `deepfried_likes`
--

INSERT INTO `deepfried_likes` (`id`, `user_id`, `deepfry_id`) VALUES
(8, 13, 29),
(13, 14, 3),
(15, 13, 4),
(46, 14, 4),
(71, 11, 36),
(72, 11, 39),
(75, 14, 36),
(76, 11, 46),
(77, 15, 46),
(78, 14, 53),
(79, 15, 47),
(80, 15, 62),
(81, 15, 63),
(82, 15, 64),
(83, 15, 65),
(84, 15, 66),
(85, 15, 76),
(86, 16, 83),
(87, 16, 82),
(88, 16, 81),
(89, 16, 80),
(90, 16, 79),
(91, 16, 78),
(92, 16, 77),
(93, 16, 76),
(94, 16, 75),
(95, 16, 74),
(96, 16, 73),
(97, 16, 72),
(98, 16, 71),
(99, 16, 70),
(100, 16, 69),
(101, 16, 68),
(102, 16, 84),
(103, 16, 85),
(104, 16, 86),
(105, 14, 84),
(106, 15, 67);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `emoji`
--

CREATE TABLE `emoji` (
  `id` int(11) NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `emoji`
--

INSERT INTO `emoji` (`id`, `code`, `tags`) VALUES
(1, '263A', ';smile;face;'),
(2, '1F914', ';think;face;'),
(3, '1F62A', ';sleep;sleepy;');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `emojify`
--

CREATE TABLE `emojify` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `emojifiedText` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `likes` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `emojify`
--

INSERT INTO `emojify` (`id`, `user_id`, `emojifiedText`, `created_at`, `likes`) VALUES
(1, 13, 'testas', '2018-01-09 16:16:21', 0),
(3, 15, 'faceâ˜º,;| k... thinkingðŸ¤”/.? sleepysvl', '2018-01-09 21:30:06', 0),
(4, 11, 'thinking test', '2018-01-10 10:18:51', 0);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `emojis_comments`
--

CREATE TABLE `emojis_comments` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `emojifiedText_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `emojis_comments`
--

INSERT INTO `emojis_comments` (`id`, `comment`, `emojifiedText_id`, `user_id`, `created_at`) VALUES
(2, 'jbh', 3, 15, '2018-01-09 22:30:22');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `emoji_likes`
--

CREATE TABLE `emoji_likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `emojifiedText_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Sukurta duomenų kopija lentelei `emoji_likes`
--

INSERT INTO `emoji_likes` (`id`, `user_id`, `emojifiedText_id`) VALUES
(13, 15, 0),
(18, 15, 13),
(20, 15, 15);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_level` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Sukurta duomenų kopija lentelei `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `user_level`, `created_at`, `status`) VALUES
(8, 'testas', 'test@test.com', '$2y$10$LOrVwnkxzThZwtD0L7.U..yVL2YkRbY9QH3MiI6Y0eY6Q0MEG5XJC', 'user', '2017-12-23 17:08:04', 'banned'),
(9, 'testas1', 'test1@test.com', '$2y$10$HGMXBN7U8.CDqem18g9z8eIPg/O9hm.U4JhrER4tWCi.mcsEG6ah2', 'user', '2017-12-23 17:36:22', 'active'),
(10, 'testas5', 'testas5@gmail.com', '$2y$10$ziLY8RlTiiY2xd4vo5VNYOqdKoyrgTKA5h4zgbHaW6OFD5SdcQlpG', 'user', '2017-12-23 18:08:25', 'active'),
(11, 'testeris', 'testas10@gmail.com', '$2y$10$A8PFmNMFOdWwpUmAqOC7.Od7EpJtM4GkXynYuNL8j9l6wPowfnQoS', 'user', '2017-12-24 20:30:48', 'active'),
(12, 'admin', 'admin@admin.com', '$2y$10$4qHMABwSPactQ6nby/B0zeYwcXK9n2LMGi0QRDZhH6nQZ6Csh7kmS', 'admin', '2017-12-27 19:23:49', 'active'),
(13, 'thot', 'mano@mailas.lt', '$2y$10$tKohkadUsuEXei0XAyJyYOiNKvNZjXbwx1KRuCDu88vRVRX7jDRI6', 'user', '2018-01-06 16:37:07', 'active'),
(14, 'chode', 'chode@chodester.com', '$2y$10$sDpBprn5hS9XKSbFZcjDvOTjt0XnWb6Jt5aqhlr4A64IMsHcBoShu', 'user', '2018-01-07 17:14:44', 'active'),
(15, 'Kokas', 'd3ivixas44@gmail.com', '$2y$10$NAgRqXKqVEzYhqaCUiE4G.6F0AL2m9sNxLItXXxcB0wsZq.iBE4fe', 'user', '2018-01-07 18:40:44', 'active'),
(16, 'cunt', 'lau@gmail.com', '$2y$10$5c190pyqFWFSQkvrS1JsouiQeO8kqIZ3LN/x5o3tGH6h0BWDQW/vG', 'user', '2018-01-07 19:09:31', 'active'),
(17, 'vvvvvvvvvvvvvvv', 'vvvvvvvvvvvvvvvvv@fdfddf.hh', '$2y$10$gZK4P03rmX/iq4ZnS2f8rux2PFNyYtrQiZ1a.O9sB.P8z6THKIc.K', 'user', '2018-01-08 15:24:01', 'active'),
(18, 'Jonas', 'Jonas@gmail.com', '$2y$10$.BnJW6OpPcNrSwOBWb5IUunySSyq1r9m2ivrQ5JuCQX4XX2A0JDc2', 'user', '2018-01-09 19:21:51', 'active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clickbaits`
--
ALTER TABLE `clickbaits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Clickbait_user_id` (`user_id`);

--
-- Indexes for table `clickbaits_comments`
--
ALTER TABLE `clickbaits_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Clickbait_comment_user_id` (`user_id`),
  ADD KEY `FK_Clickbait_comment_clickbait_id` (`clickbait_id`);

--
-- Indexes for table `clickbait_likes`
--
ALTER TABLE `clickbait_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deepfried`
--
ALTER TABLE `deepfried`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `deepfried_comments`
--
ALTER TABLE `deepfried_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deepfry_id` (`deepfry_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `deepfried_likes`
--
ALTER TABLE `deepfried_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emoji`
--
ALTER TABLE `emoji`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emojify`
--
ALTER TABLE `emojify`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `emojis_comments`
--
ALTER TABLE `emojis_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `emojifiedText_id` (`emojifiedText_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `emoji_likes`
--
ALTER TABLE `emoji_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `emojifiedText_id` (`emojifiedText_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clickbaits`
--
ALTER TABLE `clickbaits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `clickbaits_comments`
--
ALTER TABLE `clickbaits_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `clickbait_likes`
--
ALTER TABLE `clickbait_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=906;
--
-- AUTO_INCREMENT for table `deepfried`
--
ALTER TABLE `deepfried`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `deepfried_comments`
--
ALTER TABLE `deepfried_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `deepfried_likes`
--
ALTER TABLE `deepfried_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `emoji`
--
ALTER TABLE `emoji`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `emojify`
--
ALTER TABLE `emojify`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `emojis_comments`
--
ALTER TABLE `emojis_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `emoji_likes`
--
ALTER TABLE `emoji_likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- Apribojimai eksportuotom lentelėm
--

--
-- Apribojimai lentelei `clickbaits`
--
ALTER TABLE `clickbaits`
  ADD CONSTRAINT `FK_Clickbait_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Apribojimai lentelei `clickbaits_comments`
--
ALTER TABLE `clickbaits_comments`
  ADD CONSTRAINT `FK_Clickbait_comment_clickbait_id` FOREIGN KEY (`clickbait_id`) REFERENCES `clickbaits` (`id`),
  ADD CONSTRAINT `FK_Clickbait_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Apribojimai lentelei `emojify`
--
ALTER TABLE `emojify`
  ADD CONSTRAINT `emojify_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Apribojimai lentelei `emojis_comments`
--
ALTER TABLE `emojis_comments`
  ADD CONSTRAINT `emojis_comments_ibfk_1` FOREIGN KEY (`emojifiedText_id`) REFERENCES `emojify` (`id`),
  ADD CONSTRAINT `emojis_comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
