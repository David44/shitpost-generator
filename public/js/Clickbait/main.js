
var clickbaitURL = '/shitpost-generator/clickbaits';
//PRODUCTION URL
//var clickbaitURL = '/clickbaits';

// For clickbait generation options check
function clickbaitOptionsCheck() {
  if (document.getElementById('nounCheck').checked) {
      document.getElementById('ifYes').style.display = 'block';
  }
  else document.getElementById('ifYes').style.display = 'none';
}

$(document).ready(function(){
  // when the user clicks on like
  $('.like').on('click', function(){
    var clickbaitid = $(this).data('id');
    var userid = $(this).data('user');
    $clickbaitVote = $(this);

    $.ajax({
      url: clickbaitURL + '/like',
      cache: false,
      type: 'POST',
      data: {
         'liked': 1,
         'clickbaitid': clickbaitid,
         'userid': userid
      },

      success: function(response){
        
            $clickbaitVote.parent().find('span.likes_count').text(response + " likes");
            $clickbaitVote.addClass('hide');
            $clickbaitVote.siblings().removeClass('hide');
             
      }
    });
  });

  // when the user clicks on unlike
  $('.unlike').on('click', function(){

    var clickbaitid = $(this).data('id');
    var userid = $(this).data('user');
    $clickbaitVote = $(this);

    $.ajax({
      url: clickbaitURL + '/unlike',
      cache: false,
      type: 'POST',
      data: {
        'unliked': 1,
        'clickbaitid': clickbaitid,
        'userid': userid
      },
      success: function(response){
        
            $clickbaitVote.parent().find('span.likes_count').text(response + " likes");
            $clickbaitVote.addClass('hide');
            $clickbaitVote.siblings().removeClass('hide');
          
        }
      });

    });
});

