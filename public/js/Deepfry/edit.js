$(function() {

    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');

    var img = new Image();
    var reader = new FileReader();

    //var imageLoader = $('#pic');
    //imageLoader.on('change', processLoadedImg());

    document.getElementById('pic').addEventListener('change', handleFileSelect, false);

    function handleFileSelect(evt) {
        var f = evt.target.files[0]; // FileList object

        if(document.getElementById('hidden')){
            document.getElementById('hidden').id = 'unhidden';
        }


        reader.onload = function(event) {
            img.src = f;
            img.onload = function(){
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                canvas.width = img.width;
                canvas.height = img.height;
                canvas.removeAttribute("data-caman-id");
                ctx.drawImage(img,0,0, img.width, img.height);
                Caman("#canvas", img, function () {
                    this.render();
                });

            }
            img.src = reader.result;
            document.getElementById('image').src = img.src;

        }
        reader.readAsDataURL(f);
    }

    document.getElementById('pic').addEventListener('change', handleFileSelect, false);

    var $reset = $('#resetbtn');
    var $brightness = $('#brightnessbtn');
    var $noise = $('#noisebtn');
    var $sepia = $('#sepiabtn');
    var $contrast = $('#contrastbtn');
    var $color = $('#colorbtn');

    var $vintage = $('#vintagebtn');
    var $lomo = $('#lomobtn');
    var $emboss = $('#embossbtn');
    var $tiltshift = $('#tiltshiftbtn');
    var $radialblur = $('#radialblurbtn');
    var $edgeenhance = $('#edgeenhancebtn');

    var $posterize = $('#posterizebtn');
    var $clarity = $('#claritybtn');
    var $orangepeel = $('#orangepeelbtn');
    var $sincity = $('#sincitybtn');
    var $sunrise = $('#sunrisebtn');
    var $crossprocess = $('#crossprocessbtn');

    var $hazydays = $('#hazydaysbtn');
    var $love = $('#lovebtn');
    var $grungy = $('#grungybtn');
    var $jarques = $('#jarquesbtn');
    var $pinhole = $('#pinholebtn');
    var $oldboot = $('#oldbootbtn');
    var $glowingsun = $('#glowingsunbtn');

    var $hdr = $('#hdrbtn');
    var $oldpaper = $('#oldpaperbtn');
    var $pleasant = $('#pleasantbtn');

    var $upload = $('#uploadbtn');
    var $save = $('#savebtn');
    var $jpeg = $('#jpegbtn');


    /* As soon as slider value changes call applyFilters */
    $('input[type=range]').change(applyFilters);

    function applyFilters() {
        var hue = parseInt($('#hue').val());
        var cntrst = parseInt($('#contrast').val());
        var vibr = parseInt($('#vibrance').val());
        var sep = parseInt($('#sepia').val());
        var br = parseInt($('#brightness').val());
        var gm = parseFloat($('#gamma').val());
        var clip = parseInt($('#clip').val());
        var ns = parseInt($('#noise').val());

        Caman('#canvas', img, function() {
            this.revert(false);
            this.hue(hue);
            this.contrast(cntrst);
            this.vibrance(vibr);
            this.sepia(sep);
            this.brightness(br);
            this.gamma(gm);
            this.clip(clip);
            this.noise(ns);
            this.render();
        });
    }

    /* Creating custom filters */
    Caman.Filter.register("oldpaper", function() {
        this.pinhole();
        this.noise(10);
        this.orangePeel();
        this.render();
    });

    Caman.Filter.register("pleasant", function() {
        this.colorize(60, 105, 218, 10);
        this.contrast(10);
        this.sunrise();
        this.hazyDays();
        this.render();
    });

    $reset.on('click', function(e) {
        var br = parseInt($('#brush').val());
        $('input[type=range]').val(0);
        $('#gamma').val(1);
        $('#brush').val(br);
        Caman('#canvas', img, function() {
            this.revert();
            this.render();
        });

    });


    $vintage.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.vintage().render();
        });
    });

    $lomo.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.lomo().render();
        });
    });

    $emboss.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.emboss().render();
        });
    });

    $tiltshift.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.tiltShift({
                angle: 90,
                focusWidth: 600
            }).render();
        });
    });

    $radialblur.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.radialBlur().render();
        });
    });

    $edgeenhance.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.edgeEnhance().render();
        });
    });

    $posterize.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.posterize(8, 8).render();
        });
    });

    $clarity.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.clarity().render();
        });
    });

    $orangepeel.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.orangePeel().render();
        });
    });

    $sincity.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.sinCity().render();
        });
    });

    $sunrise.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.sunrise().render();
        });
    });

    $crossprocess.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.crossProcess().render();
        });
    });

    $love.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.love().render();
        });
    });

    $grungy.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.grungy().render();
        });
    });

    $jarques.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.jarques().render();
        });
    });

    $pinhole.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.pinhole().render();
        });
    });

    $oldboot.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.oldBoot().render();
        });
    });

    $glowingsun.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.glowingSun().render();
        });
    });

    $hazydays.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.hazyDays().render();
        });
    });

    /* Calling multiple filters inside same function */
    $hdr.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.contrast(10);
            this.contrast(10);
            this.jarques();
            this.render();
        });
    });

    /* Custom filters that we created */
    $oldpaper.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.oldpaper();
            this.render();
        });
    });

    $pleasant.on('click', function(e) {
        Caman('#canvas', img, function() {
            this.pleasant();
            this.render();
        });
    });

    $save.on('click', function(e) {

        Caman("#canvas", function() {
            //console.log(this.toBase64());
            var a  = document.createElement('a');
            a.href = this.toBase64();
            a.download = "result.png";
            a.click();
        });
    });

});
