
var emojiURL = '/shitpost-generator/emojis';
//PRODUCTION URL
//var emojiURL = '/emojis';

// For clickbait generation options check
/*function clickbaitOptionsCheck() {
  if (document.getElementById('nounCheck').checked) {
      document.getElementById('ifYes').style.display = 'block';
  }
  else document.getElementById('ifYes').style.display = 'none';
}*/

$(document).ready(function(){
  // when the user clicks on like
  $('.like').on('click', function(){
    var emojiid = $(this).data('id');
    var userid = $(this).data('user');
    $emojiVote = $(this);

    $.ajax({
      url: emojiURL + '/like',
      cache: false,
      type: 'POST',
      data: {
         'liked': 1,
         'emojiid': emojiid,
         'userid': userid
      },

      success: function(response){
        
            $emojiVote.parent().find('span.likes_count').text(response + " likes");
            $emojiVote.addClass('hide');
            $emojiVote.siblings().removeClass('hide');
             
      }
    });
  });

  // when the user clicks on unlike
  $('.unlike').on('click', function(){

    var emojiid = $(this).data('id');
    var userid = $(this).data('user');
    $emojiVote = $(this);

    $.ajax({
      url: emojiURL + '/unlike',
      cache: false,
      type: 'POST',
      data: {
        'unliked': 1,
        'emojiid': emojiid,
        'userid': userid
      },
      success: function(response){
        
            $emojiVote.parent().find('span.likes_count').text(response + " likes");
            $emojiVote.addClass('hide');
            $emojiVote.siblings().removeClass('hide');
          
        }
      });

    });
});

